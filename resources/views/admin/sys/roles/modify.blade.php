<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include("admin.public.head")
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 20px;">
                <form class="layui-form" name="myForm">
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            <span class="x-red">*</span>名称
                        </label>
                        <div class="layui-input-inline">
                          <input type="text" value="{{$model['title']}}" name="title" lay-verify="required"  autocomplete="off" placeholder="请输入名称" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">描述</label>
                        <div class="layui-input-block ml0">
                          <textarea name="description" placeholder="请输入描述" class="layui-textarea">{{$model['description']}}</textarea>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">权限</label>
                        <div class="layui-input-block ml0">
                            <div id="tree"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label"></label>
                        <button type="button" class="layui-btn" id="edit">提交</button>
                    </div>
                </form>
            </div>
        </div>
        <script>
            layui.config({
                base: '/static/admin/lib/'
            }).extend({
                treeTable: 'treeTable/treeTable'
            }).use(['layer', 'util', 'treeTable', 'form'], function () {
                var $ = layui.jquery,
                    layer = layui.layer,
                    util = layui.util,
                    table = layui.treeTable,
                    form = layui.form;

                // 渲染表格
                var insTb = table.render({
                    elem: '#tree',
                    id: "treeReload",
                    tree: {
                        iconIndex: 1,
                        arrowType: 'arrow2',
                    },
                    text: {},
                    cols: [
                        {type: 'checkbox'},
                        {field: 'menu_name', title: '名称'},
                    ],
                    reqData: function (data, callback) {
                        var url = '{{url("admin/admin.system/index")}}';
                        $.post(url, function (res) {
                            callback(res);
                            insTb.setChecked([{{$model['rules']}}]);  // 设置选中数据
                        });
                    },
                });

                setTimeout(function () {
                    $('body').children('.page-loading').hide();
                    $('body').removeClass('page-no-scroll');
                }, 100);

                //监听提交
                $("#edit").on("click",function(){
                    var that = $(this);
                    var data = insTb.checkStatus();
                    var param = {
                        ids: tableHandle(data),
                        id: "{{$model['id']}}",
                        title: $('input[name=title]').val(),
                        description: $('textarea[name=description]').val(),
                    };
                    $.ajax({
                        type: "POST",
                        data: param,
                        dataType:"JSON",
                        url: "{{url('admin/admin.role/roles_update')}}",
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    xadmin.close();
                                    xadmin.father_reload();
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            });
        </script>
        @include("admin.public.jsFile")
    </body>
</html>
