<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 10px;">
                <div class="layui-tab layui-tab-card">
                    <ul class="layui-tab-title">
                        @foreach($list as $k => $v)
                            <a href="@if($k == $type) javascript:; @else {{url('admin/config/index')}}?type={{$k}} @endif"><li @if ($k == $type) class="layui-this" @endif>{{$v}}</li></a>
                        @endforeach
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <form class="layui-form" name="myForm">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        APPID
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="tencent_live_appid" value="{{$model['tencent_live_appid']}}" lay-verify="required"  autocomplete="off" placeholder="云直播APPID" class="layui-input">
                                        <span class="x-red">腾讯云 云直播APPID</span>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        secretId
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="tencent_live_secretid" value="{{$model['tencent_live_secretid']}}" lay-verify="required"  autocomplete="off" placeholder="云直播secretId" class="layui-input">
                                        <span class="x-red">腾讯云 云直播secretId</span>
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">
                                        secretKey
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="password" name="tencent_live_secretkey" value="{{$model['tencent_live_secretkey']}}" lay-verify="required"  autocomplete="off" placeholder="云直播secretKey" class="layui-input">
                                        <span class="x-red">腾讯云 云直播secretKey</span>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        身份标识
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="tencent_im_identifier" value="{{$model['tencent_im_identifier']}}" lay-verify="required"  autocomplete="off" placeholder="IM即时通讯身份，如：administrators" class="layui-input">
                                        <span class="x-red">腾讯云 IM即时通讯身份</span>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        SDKAppID
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="tencent_im_sdkappid" value="{{$model['tencent_im_sdkappid']}}" lay-verify="required"  autocomplete="off" placeholder="IM即时通讯SDKAppID" class="layui-input">
                                        <span class="x-red">腾讯云 IM即时通讯SDKAppID</span>
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">
                                        Key
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="password" name="tencent_im_key" value="{{$model['tencent_im_key']}}" lay-verify="required"  autocomplete="off" placeholder="IM即时通讯Key" class="layui-input">
                                        <span class="x-red">腾讯云 IM即时通讯Key</span>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <input type="hidden" name="type" value="{{$type}}">
                                    <label for="L_repass" class="layui-form-label"></label>
                                    <button class="layui-btn" lay-filter="edit" lay-submit="">提交</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            layui.use(['layer', 'form'], function () {
                var $ = layui.jquery,
                    layer = layui.layer,
                    form = layui.form;

                //监听提交
                form.on('submit(edit)',function(data) {
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: $('form[name=myForm]').serialize(),
                        dataType:"JSON",
                        url: '{{url('admin/config/update')}}',
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    window.location.href = "";
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            });
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
