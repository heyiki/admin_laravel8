<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 10px;">
                <div class="layui-tab layui-tab-card">
                    <ul class="layui-tab-title">
                        @foreach($list as $k => $v)
                            <a href="@if($k == $type) javascript:; @else {{url('admin/config/index')}}?type={{$k}} @endif"><li @if ($k == $type) class="layui-this" @endif>{{$v}}</li></a>
                        @endforeach
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <form class="layui-form" name="myForm">
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">账号</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="account" value="{{$model['account']}}" lay-verify="required"  autocomplete="off" placeholder="账号" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">秘钥</label>
                                    <div class="layui-input-block ml0">
                                        <input type="password" name="appcode" value="{{$model['appcode']}}" lay-verify="required"  autocomplete="off" placeholder="appCode" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        物流URL
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="logistics_api_url" value="{{$model['logistics_api_url']}}" lay-verify="required"  autocomplete="off" placeholder="物流API请求地址" class="layui-input">
                                        <div class="x-red">物流API请求地址</div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        银行卡URL
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="bank_api_url" value="{{$model['bank_api_url']}}" lay-verify="required"  autocomplete="off" placeholder="银行卡三要素API请求地址" class="layui-input">
                                        <div class="x-red">银行卡三要素API请求地址</div>
                                    </div>
                                </div>
                                <!--阿里短信服务-->
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">短信ID</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="sms_accesskey_id" value="{{$model['sms_accesskey_id']}}" lay-verify="required"  autocomplete="off" placeholder="AccessKey ID" class="layui-input">
                                        <div class="x-red">短信 AccessKey ID</div>
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">短信密钥</label>
                                    <div class="layui-input-block ml0">
                                        <input type="password" name="sms_accesskey_secret" value="{{$model['sms_accesskey_secret']}}" lay-verify="required"  autocomplete="off" placeholder="AccessKey Secret" class="layui-input">
                                        <div class="x-red">短信 AccessKey Secret</div>
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">短信签名</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="sms_sign_name" value="{{$model['sms_sign_name']}}" lay-verify="required"  autocomplete="off" placeholder="短信签名名称" class="layui-input">
                                        <div class="x-red">短信签名名称</div>
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">短信模板</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="sms_template_code" value="{{$model['sms_template_code']}}" lay-verify="required"  autocomplete="off" placeholder="短信模板CODE" class="layui-input">
                                        <div class="x-red">短信模板CODE</div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <input type="hidden" name="type" value="{{$type}}">
                                    <label for="L_repass" class="layui-form-label"></label>
                                    <button class="layui-btn" lay-filter="edit" lay-submit="">提交</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            layui.use(['layer', 'form'], function () {
                var $ = layui.jquery,
                    layer = layui.layer,
                    form = layui.form;

                //监听提交
                form.on('submit(edit)',function(data) {
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: $('form[name=myForm]').serialize(),
                        dataType:"JSON",
                        url: '{{url('admin/config/update')}}',
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    window.location.href = "";
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            });
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
