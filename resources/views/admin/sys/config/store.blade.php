<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 10px;">
                <div class="layui-tab layui-tab-card">
                    <ul class="layui-tab-title">
                        @foreach($list as $k => $v)
                            <a href="@if($k == $type) javascript:; @else {{url('admin/config/index')}}?type={{$k}} @endif"><li @if ($k == $type) class="layui-this" @endif>{{$v}}</li></a>
                        @endforeach
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <form class="layui-form" name="myForm">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        运费
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="freight_fee" value="{{$model['freight_fee'] ? : 0}}" lay-verify="required"  autocomplete="off" placeholder="购买商品运费" class="layui-input" onkeyup="this.value=this.value.replace(/^(\d*\.?\d{0,2}).*/,'$1')">
                                    </div>
                                    <p class="x-red">单位：元，购买商品的运费，0表示包邮</p>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        库存预警
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="number" name="stock_warning" value="{{$model['stock_warning'] ? : 20}}" lay-verify="required"  autocomplete="off" placeholder="库存预警" class="layui-input">
                                    </div>
                                </div>
                                <!-- <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">积分抵扣</label>
                                    <div class="layui-input-inline">
                                        <input type="number" name="integral_deduction" value="{$model.integral_deduction ? : 1000}" lay-verify="required"  autocomplete="off" placeholder="单位：积分" class="layui-input">
                                        <p class="x-red">下单时使用积分抵扣，积分抵扣:1，如：每1000积分，可以扣款1元</p>
                                    </div>
                                    <div class="layui-input-inline">
                                        <input type="radio" name="is_integral_deduction" value="1" title="开启抵扣" checked="">
                                        <input type="radio" name="is_integral_deduction" value="2" title="关闭抵扣">
                                    </div>
                                </div> -->
                                <!-- <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">购买积分</label>
                                    <div class="layui-input-block ml0">
                                        <input type="number" name="amount_change" value="{$model.amount_change ? : 100}" lay-verify="required"  autocomplete="off" placeholder="单位：积分" class="layui-input">
                                    </div>
                                    <p class="x-red">购买商品积分比率，如：用户每次消费100元，可以转化为1积分</p>
                                </div> -->

                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        取消订单
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="number" name="over_time" value="{{$model['over_time'] ? : 1}}" lay-verify="required"  autocomplete="off" placeholder="单位：小时，系统自动取消超时未支付订单" class="layui-input">
                                    </div>
                                    <p class="x-red">单位：小时，系统自动取消超时未支付订单</p>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">自动收货</label>
                                    <div class="layui-input-block ml0">
                                        <input type="number" name="confirm_time" value="{{$model['confirm_time'] ? : 15}}" lay-verify="required"  autocomplete="off" placeholder="单位：天，系统自动确认收货" class="layui-input">
                                    </div>
                                    <p class="x-red">单位：天，系统自动确认收货</p>
                                </div>
                                <!-- <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">自动评价</label>
                                    <div class="layui-input-block ml0">
                                        <input type="number" name="reply_time" value="{{$model['reply_time'] ? : 15}}" lay-verify="required"  autocomplete="off" placeholder="单位：天，系统自动评价" class="layui-input">
                                    </div>
                                    <p class="x-red">单位：天，系统自动评价</p>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">地址限制</label>
                                    <div class="layui-input-block ml0">
                                        <input type="number" name="address_limit" value="{{$model['address_limit'] ? : 10}}" lay-verify="required"  autocomplete="off" placeholder="用户填写收货地址数量限制" class="layui-input" onkeyup="this.value=this.value.replace(/\D/g,'')">
                                    </div>
                                    <p class="x-red">用户填写收货地址数量限制</p>
                                </div> -->
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">退货理由</label>
                                    <div class="layui-input-block ml0">
                                        <textarea name="return_desc" lay-verify="required" placeholder="多个退货理由以;分隔" class="layui-textarea">{{$model['return_desc']}}</textarea>
                                    </div>
                                </div>
                                <!-- <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">保障</label>
                                    <div class="layui-input-block ml0">
                                        <textarea name="ensure" lay-verify="required" placeholder="多个保障以;分隔" class="layui-textarea">{{$model['ensure']}}</textarea>
                                        <p class="x-red">格式如，正品保证：本商品是官方正品;包邮：本商品属包邮商品，可放心使用</p>
                                    </div>
                                </div> -->
                                <!-- <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">售后公告</label>
                                    <div class="layui-input-block ml0">
                                        <textarea name="refund_tips" lay-verify="required" placeholder="多个保障以;分隔" class="layui-textarea">{{$model['refund_tips']}}}</textarea>
                                        <p class="x-red">格式如，未与商家协商一致，请勿使用到付或平邮，以免商家拒收货物;交易的钱还在平台中间账号，确保你的资金安全;填写真实的物流信息，逾期未填写，退货申请将关闭</p>
                                    </div>
                                </div> -->
                                <div class="layui-form-item">
                                    <input type="hidden" name="type" value="{{$type}}">
                                    <label for="L_repass" class="layui-form-label"></label>
                                    <button class="layui-btn" lay-filter="edit" lay-submit="">提交</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            layui.use(['layer', 'form'], function () {
                var $ = layui.jquery,
                    layer = layui.layer,
                    form = layui.form;

                //监听提交
                form.on('submit(edit)',function(data) {
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: $('form[name=myForm]').serialize(),
                        dataType:"JSON",
                        url: '{{url('admin/config/update')}}',
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    window.location.href = "";
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            });
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
