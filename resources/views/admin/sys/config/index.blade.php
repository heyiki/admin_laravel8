<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 10px;">
                <div class="layui-tab layui-tab-card">
                    <ul class="layui-tab-title">
                        @foreach($list as $k => $v)
                            <a href="@if($k == $type) javascript:; @else {{url('admin/config/index')}}?type={{$k}} @endif"><li @if ($k == $type) class="layui-this" @endif>{{$v}}</li></a>
                        @endforeach
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <form class="layui-form" name="myForm">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        网站名称
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="seo_title" value="{{$model['seo_title']}}" lay-verify="required"  autocomplete="off" placeholder="网站名称" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">网站关键词</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="seo_keywords" value="{{$model['seo_keywords']}}" lay-verify="required"  autocomplete="off" placeholder="如：关键词,关键词1,关键词2" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">网站描述</label>
                                    <div class="layui-input-block ml0">
                                        <textarea name="seo_description" lay-verify="required" placeholder="网站描述" class="layui-textarea">{{$model['seo_description']}}</textarea>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        ICON
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <div class="fc-upload" id="icon">
                                            @if ($model['icon'])
                                                @foreach($model['icon'] as $v)
                                                    <div class="fc-files">
                                                        <img src="{{$v}}">
                                                        <div class="fc-upload-cover">
                                                            <i class="ivu-icon ivu-icon-ios-eye-outline"></i>
                                                            <i class="ivu-icon ivu-icon-ios-trash-outline"></i>
                                                        </div>
                                                        <input type="hidden" name="icon[]" value="{{$v}}">
                                                    </div>
                                                @endforeach
                                            @endif
                                            <div class="fc-upload-btn" onclick="xadmin.open('选择文件','{{url('admin/images/index')}}?catalog=config&field=icon&limit=1','50%','65%')">
                                                <i class="ivu-icon ivu-icon-images" style="font-size: 20px;"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        LOGO
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <div class="fc-upload" id="logo">
                                            @if ($model['logo'])
                                                @foreach($model['logo'] as $v)
                                                    <div class="fc-files">
                                                        <img src="{{$v}}">
                                                        <div class="fc-upload-cover">
                                                            <i class="ivu-icon ivu-icon-ios-eye-outline"></i>
                                                            <i class="ivu-icon ivu-icon-ios-trash-outline"></i>
                                                        </div>
                                                        <input type="hidden" name="logo[]" value="{{$v}}">
                                                    </div>
                                                @endforeach
                                            @endif
                                            <div class="fc-upload-btn" onclick="xadmin.open('选择文件','{{url('admin/images/index')}}?catalog=config&field=logo&limit=1','50%','65%')">
                                                <i class="ivu-icon ivu-icon-images" style="font-size: 20px;"></i>
                                            </div>
                                            <p class="x-red">请上传logo</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        电话
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="tel" value="{{$model['tel']}}" lay-verify="required"  autocomplete="off" placeholder="电话/手机号" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        邮箱
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="mail" value="{{$model['mail']}}" lay-verify="required"  autocomplete="off" placeholder="邮箱" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        地址
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="address" value="{{$model['address']}}" lay-verify="required"  autocomplete="off" placeholder="地址" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        备案号
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="record" value="{{$model['record']}}" autocomplete="off" placeholder="备案号" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        版权
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="copyright" value="{{$model['copyright']}}" autocomplete="off" placeholder="版权" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        当前版本
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="edition" value="{{$model['edition']}}" autocomplete="off" placeholder="当前版本" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <input type="hidden" name="type" value="{{$type}}">
                                    <label for="L_repass" class="layui-form-label"></label>
                                    <button class="layui-btn" lay-filter="edit" lay-submit="">提交</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            layui.use(['layer', 'form'], function () {
                var $ = layui.jquery,
                    layer = layui.layer,
                    form = layui.form;

                //监听提交
                form.on('submit(edit)',function(data) {
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: $('form[name=myForm]').serialize(),
                        dataType:"JSON",
                        url: '{{url('admin/config/update')}}',
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    window.location.href = "";
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            });
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
