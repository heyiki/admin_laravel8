<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 10px;">
                <div class="layui-tab layui-tab-card">
                    <ul class="layui-tab-title">
                        @foreach($list as $k => $v)
                            <a href="@if($k == $type) javascript:; @else {{url('admin/config/index')}}?type={{$k}} @endif"><li @if ($k == $type) class="layui-this" @endif>{{$v}}</li></a>
                        @endforeach
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <form class="layui-form" name="myForm">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        SMTP
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="smtp_server" value="{{$model['smtp_server']}}" lay-verify="required"  autocomplete="off" placeholder="发送邮箱的smtp地址。如: smtp.gmail.com或smtp.qq.com或smtp.163.com" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        端口
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="smtp_port" value="{{$model['smtp_port']}}" lay-verify="required"  autocomplete="off" placeholder="smtp的端口。默认为25。具体请参看各STMP服务商的设置说明 （如果使用Gmail，请将端口设为465）" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        发件人名称
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="smtp_name" value="{{$model['smtp_name']}}" lay-verify="required"  autocomplete="off" placeholder="发件人名称" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        邮箱账号
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="smtp_account" value="{{$model['smtp_account']}}" lay-verify="required"  autocomplete="off" placeholder="使用发送邮件的邮箱账号" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        邮箱密码
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="password" name="smtp_password" value="{{$model['smtp_password']}}" lay-verify="required"  autocomplete="off" placeholder="使用发送邮件的邮箱密码/授权码。具体请参看各STMP服务商的设置说明" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <input type="hidden" name="type" value="{{$type}}">
                                    <label for="L_repass" class="layui-form-label"></label>
                                    <button class="layui-btn" lay-filter="edit" lay-submit="">提交</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            layui.use(['layer', 'form'], function () {
                var $ = layui.jquery,
                    layer = layui.layer,
                    form = layui.form;

                //监听提交
                form.on('submit(edit)',function(data) {
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: $('form[name=myForm]').serialize(),
                        dataType:"JSON",
                        url: '{{url('admin/config/update')}}',
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    window.location.href = "";
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            });
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
