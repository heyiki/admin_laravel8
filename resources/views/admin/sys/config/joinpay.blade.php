<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 10px;">
                <div class="layui-tab layui-tab-card">
                    <ul class="layui-tab-title">
                        @foreach($list as $k => $v)
                            <a href="@if($k == $type) javascript:; @else {{url('admin/config/index')}}?type={{$k}} @endif"><li @if ($k == $type) class="layui-this" @endif>{{$v}}</li></a>
                        @endforeach
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <form class="layui-form" name="myForm">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">
                                        商户名称
                                    </label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="MERCHANT_NAME" value="{{$model['MERCHANT_NAME']}}" lay-verify="required"  autocomplete="off" placeholder="商户名称" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">商户号</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="MERCHANT_NO" value="{{$model['MERCHANT_NO']}}" lay-verify="required"  autocomplete="off" placeholder="商户号" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">报备商户号</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="TradeMerchantNo" value="{{$model['TradeMerchantNo']}}" lay-verify="required"  autocomplete="off" placeholder="报备商户号" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">商户密钥</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="KEY_MD5" value="{{$model['KEY_MD5']}}" lay-verify="required"  autocomplete="off" placeholder="商户密钥" class="layui-input">
                                    </div>
                                </div>
                                <!-- <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">微信公众号APPID</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="gzh_appid" value="{{$model['gzh_appid']}}" lay-verify=""  autocomplete="off" placeholder="微信公众号APPID" class="layui-input">
                                    </div>
                                </div> -->
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">微信小程序APPID</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="xcx_appid" value="{{$model['xcx_appid']}}" lay-verify="required"  autocomplete="off" placeholder="微信小程序APPID" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">支付地址</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="NotifyUrl" value="{{$model['NotifyUrl']}}" lay-verify="required"  autocomplete="off" placeholder="支付异步通知地址" class="layui-input">
                                        <span class="x-red">完整异步通知地址，如：https://xxx.com/notice/index.html</span>
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">退款地址</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="RefundNotifyUrl" value="{{$model['RefundNotifyUrl']}}" lay-verify="required"  autocomplete="off" placeholder="退款异步通知地址" class="layui-input">
                                        <span class="x-red">完整异步通知地址，如：https://xxx.com/notice/index.html</span>
                                    </div>
                                </div>
                                <!-- <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">代付地址</label>
                                    <div class="layui-input-block ml0">
                                        <input type="text" name="paidNotifyUrl" value="{$model.paidNotifyUrl}" lay-verify=""  autocomplete="off" placeholder="代付通知地址" class="layui-input">
                                        <span class="x-red">完整异步通知地址，如：https://yiwei.com/notice/index.html</span>
                                    </div>
                                </div> -->

                                <div class="layui-form-item">
                                    <input type="hidden" name="type" value="{{$type}}">
                                    <label for="L_repass" class="layui-form-label"></label>
                                    <button class="layui-btn" lay-filter="edit" lay-submit="">提交</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            layui.use(['layer', 'form'], function () {
                var $ = layui.jquery,
                    layer = layui.layer,
                    form = layui.form;

                //监听提交
                form.on('submit(edit)',function(data) {
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: $('form[name=myForm]').serialize(),
                        dataType:"JSON",
                        url: '{{url('admin/config/update')}}',
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    window.location.href = "";
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            });
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
