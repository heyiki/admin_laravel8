<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    @include('admin.public.head')
</head>
<body>
	<div class="x-nav">
		<a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
        </a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">
                            <button class="layui-btn" onclick="xadmin.open('添加规则','{{url('admin/admin.system/create')}}','70%','80%')">
                            	<i class="layui-icon"></i>添加
                            </button>
                        </div>
                        <!-- 表格 -->
                        <div id="tree"></div>
						<!-- 表格状态列 -->
						<script type="text/html" id="status">
						    <input type="checkbox" lay-filter="status" lay-skin="switch" value="@{{d.id}}" data-table="system_menus" data-pk="id" data-field="is_show" lay-text="显示|隐藏" @{{d.is_show == 1 ? 'checked' : ''}}/>
						</script>
						<!-- 表格操作列 -->
						<script type="text/html" id="operate">
						    <a class="layui-btn layui-btn-sm" lay-event="edit" onclick="xadmin.open('编辑规则','{{url('admin/admin.system/create')}}?id=@{{d.id}}','70%','80%')">编辑</a>
						    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
						</script>
                    </div>
                </div>
            </div>
	</div>
    @include('admin.public.jsFile')
	<script>
    	layui.config({
	        base: '/static/admin/lib/'
	    }).extend({
	        treeTable: 'treeTable/treeTable'
	    }).use(['layer', 'util', 'treeTable', 'form'], function () {
	        var $ = layui.jquery,
	        	layer = layui.layer,
	        	util = layui.util,
	        	table = layui.treeTable,
	        	form = layui.form;

	        // 渲染表格
	        var insTb = table.render({
	            elem: '#tree',
	            tree: {
	                iconIndex: 1,
	                arrowType: 'arrow2',
	                openName: 'open',
	            },
	            text: {},
	            cols: [
	                {field: 'id', title: 'ID', width: '100', align: 'center'},
	                {field: 'menu_name', title: '名称'},
	                {field: 'controller', title: '控制器'},
	                {field: 'action', title: '方法名'},
	                {field: 'params', title: '参数'},
	                {
	                	field: 'icon', title: '图标', templet: function (d) {
	                        return d.icon;
	                    }
	                },
	                {templet: '#status', title: '菜单状态', width: 100},
	                {align: 'center', toolbar: '#operate', title: '操作', width: '10%'}
	            ],
                done: function(res, curr, count) {
                    $("table").width("100%");
                },
	            reqData: function (data, callback) {
	                setTimeout(function () {  // 故意延迟一下
	                    var url = '{{url("admin/admin.system/index")}}';
	                    $.post(url, function (res) {
	                        callback(res);
	                    });
	                }, 100);
	            },
	        });

	        setTimeout(function () {
	            $('body').children('.page-loading').hide();
	            $('body').removeClass('page-no-scroll');
	        }, 100);

	        //删除
			table.on('tool(tree)', function(obj){
			   	if(obj.event === 'del'){
			   		var id = obj.data.id;
			   		if(!id){
			   			layer.msg("系统繁忙，请重试",{icon: 2, time: 1000, anim: 6});
			   			return false;
			   		}
					layer.confirm('您确定要删除？', function(index){
						$.post('{{url("admin/admin.system/delete")}}', {id: id}, function(res) {
							if(res.code == 200){
								layer.msg(res.msg,{icon: 1, time: 1000, anim: 6},function(){
									obj.del();
								});
							}else{
								layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
							}
						});
						layer.close(index);
					});
			    }
			    return false;
			});

	    });
	</script>
</body>
</html>
