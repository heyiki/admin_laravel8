<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include("admin.public.head")
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 20px;">
                <form class="layui-form" name="myForm">
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            姓名
                        </label>
                        <div class="layui-input-inline">
                          <input type="text" value="{{$model['real_name']}}" name="real_name" lay-verify="required"  autocomplete="off" placeholder="请输入姓名" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            账号
                        </label>
                        <div class="layui-input-inline">
                          <input type="text" value="{{$model['account']}}" name="account" lay-verify="required" autocomplete="off" placeholder="请输入账号"  class="layui-input @if($model['id'])layui-disabled @endif" @if($model['id'])readonly="readonly" @endif>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            密码
                        </label>
                        <div class="layui-input-inline">
                          <input type="text" name="password" autocomplete="off" placeholder="请输入密码" class="layui-input">
                        </div>
                        <div class="layui-form-mid layui-word-aux">
                            <span class="x-red">*</span>为空时不修改密码
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            角色
                        </label>
                        <div class="layui-input-inline">
                            <select name="roles_id" lay-verify="required" lay-search="">
                                <option value="">请选择</option>
                                @foreach($roles as $v)
                                <option value="{{$v['id']}}">{{$v['title']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="layui-form-mid layui-word-aux">
                            <span class="x-red">*</span>直接选择或搜索选择
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <input type="hidden" name="id" value="{{$model['id']}}">
                        <label for="L_repass" class="layui-form-label"></label>
                        <button type="button" class="layui-btn" id="edit">提交</button>
                    </div>
                </form>
            </div>
        </div>
        <script>
            $(function() {
                $("select[name=roles_id] option[value='{{$model['roles_id']}}']").attr('selected','selected');
                layui.use(['layer', 'form'], function () {
                    var $ = layui.jquery,
                        layer = layui.layer,
                        form = layui.form;
                    //监听提交
                    $("#edit").on("click",function(){
                        var that = $(this);
                        $.ajax({
                            type: "POST",
                            data: $("form[name=myForm]").serialize(),
                            dataType:"JSON",
                            url: "{{url('admin/admin/edit')}}",
                            beforeSend: function () {
                                that.attr("disabled","disabled");
                                layer.load(2);
                            },
                            success: function (res) {
                                if(res.code == 200){
                                    layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                        xadmin.close();
                                        xadmin.father_reload();
                                    });
                                }else{
                                    layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                    return false;
                                }
                            },
                            complete: function () {
                                that.removeAttr("disabled");
                                layer.closeAll('loading');
                            },
                            error: function (e) {
                                console.info("Error："+e);
                            }
                        });
                    })
                });
            })
        </script>
        @include("admin.public.jsFile")
    </body>
</html>
