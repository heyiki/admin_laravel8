<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    @include("admin.public.head")
</head>
<body>
    <div class="x-nav">
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
        </a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <!-- 表格 -->
                    <div id="table" lay-filter="table"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            layui.use(['table', 'form'], function () {
                var $ = layui.jquery,
                    table = layui.table,
                    form = layui.form;

                // 渲染表格
                var insTb = table.render({
                    elem: '#table',
                    id: "searchReload",
                    cellMinWidth: 80,
                    url: '{{url('admin/admin/log')}}',
                    page: {
                        layout: ['prev', 'page', 'next', 'skip', 'count'],
                        groups: 5,
                    },
                    limit: "{{$pageSize}}",
                    cols: [[
                        {align: 'center',field:'id', title: 'ID', sort: true, width: 100},
                        {
                            align: 'center', field: 'admin_id', title: '管理员', width: "10%", templet: function (d) {
                                return d.admin_id +" / " + d.admin_name;
                            }
                        },
                        {align: 'center',field: 'path', title: '链接'},
                        {align: 'center',field: 'ip', title: '登录IP', width: "10%"},
                        {align: 'center',field: 'create_time', title: '操作时间', width: "10%"},
                    ]],
                    done: function(res, curr, count) {
                        $("table").width("100%");
                    },
                    reqData: function (res) {
                        console.log(res)
                    },
                });
            });
        })
    </script>
</body>
</html>
