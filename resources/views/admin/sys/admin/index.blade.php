<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    @include("admin.public.head")
</head>
<body>
    <div class="x-nav">
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
        </a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5" id="searchForm">
                            <div class="layui-input-inline layui-show-xs-block">
                                <input type="text" name="account" placeholder="账号" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-input-inline layui-show-xs-block">
                                <input type="text" name="real_name" placeholder="姓名" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-input-inline layui-show-xs-block">
                                <button type="button" class="layui-btn" id="search">
                                    <i class="layui-icon">&#xe615;</i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="layui-card-header">
                        <button class="layui-btn" onclick="xadmin.open('添加管理员','{{url("admin/admin/create")}}','70%','90%')">
                            <i class="layui-icon"></i>添加
                        </button>
                    </div>
                    <!-- 表格 -->
                    <div id="table" lay-filter="table"></div>
                    <!-- 表格状态列 -->
                    <script type="text/html" id="status">
                        <input type="checkbox" lay-filter="status" lay-skin="switch" value="@{{d.id}}" data-table="admin" data-pk="id" data-field="status" lay-text="启用|禁用" @{{d.status == 1 ? 'checked' : ''}}/>
                    </script>
                    <!-- 表格操作列 -->
                    <script type="text/html" id="operate">
                        <a class="layui-btn layui-btn-sm" onclick="xadmin.open('编辑管理员','{{url("admin/admin/create")}}?id=@{{d.id}}','70%','80%')">编辑</a>
                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                    </script>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            layui.use(['table', 'form'], function () {
                var $ = layui.jquery,
                    table = layui.table,
                    form = layui.form;

                // 渲染表格
                var insTb = table.render({
                    elem: '#table',
                    id: "searchReload",
                    cellMinWidth: 80,
                    url: '{{url('admin/admin/index')}}',
                    page: {
                        layout: ['prev', 'page', 'next', 'skip', 'count'],
                        groups: 5,
                    },
                    limit: {{$pageSize}},
                    cols: [[
                        {type:'checkbox', width: 80},
                        {align: 'center',field:'id', title: 'ID', sort: true, width: 100},
                        {align: 'center',field: 'real_name', title: '姓名'},
                        {align: 'center',field: 'account', title: '账号'},
                        {align: 'center',field: 'roles_name', title: '角色'},
                        {align: 'center',field: 'last_ip', title: '登录IP'},
                        {align: 'center',field: 'last_time', title: '登录时间'},
                        {align: 'center',field: 'login_count', title: '登录次数'},
                        {align: 'center',field: 'create_time', title: '创建时间'},
                        {align: 'center',templet: '#status', title: '状态', width: "8%"},
                        {align: 'center', toolbar: '#operate', title: '操作', width: "10%"}
                    ]],
                    done: function(res, curr, count) {
                        $("table").width("100%");
                    },
                    reqData: function (res) {
                        console.log(res)
                    },
                });

                //删除
                table.on('tool(table)', function(obj){
                    if(obj.event === 'del'){
                        var id = obj.data.id;
                        if(!id){
                            layer.msg("系统繁忙，请重试",{icon: 2, time: 1000, anim: 6});
                            return false;
                        }
                        layer.confirm('您确定要删除？', function(index){
                            $.post('{{url("admin/admin/delete")}}', {id: id}, function(res) {
                                if(res.code == 200){
                                    layer.msg(res.msg,{icon: 1, time: 1000, anim: 6},function(){
                                        obj.del();
                                    });
                                }else{
                                    layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
                                }
                            });
                            layer.close(index);
                        });
                    }
                    return false;
                });
            });
        })
    </script>
    @include("admin.public.jsFile")
</body>
</html>
