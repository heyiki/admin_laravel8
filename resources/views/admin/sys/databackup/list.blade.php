<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    @include('admin.public.head')
</head>
<body>
    <div class="x-nav">
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
        </a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">
                        <button type="button" class="layui-btn backup" data-type="backup">备份</button>
                        <button type="button" class="layui-btn layui-btn-warm repair" data-type="repair">修复</button>
                        <button type="button" class="layui-btn layui-btn-normal optimize" data-type="optimize">优化</button>
                    </div>
                    <!-- 表格 -->
                    <div id="table" lay-filter="table"></div>
                    <!-- 表格操作列 -->
                    {{--<!-- <script type="text/html" id="operate">
                        <a class="layui-btn layui-btn-sm" onclick="xadmin.open('查看详情','{:url('sys.admin/create')}?id={{d.id}}','70%','80%')">详情</a>
                    </script> -->--}}
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            layui.use(['table', 'form'], function () {
                var $ = layui.jquery,
                    table = layui.table,
                    form = layui.form;

                // 渲染表格
                var insTb = table.render({
                    elem: '#table',
                    id: "searchReload",
                    cellMinWidth: 80,
                    url: '{{url("admin/databackup/create")}}',
                    page: {
                        layout: ['prev', 'page', 'next', 'skip', 'count'],
                        groups: 5,
                    },
                    limit: {{$total}},
                    cols: [[
                        {type:'checkbox', width: 80},
                        {align: 'center',field: 'name', title: '表名称', sort: true},
                        {align: 'center',field: 'comment', title: '备注'},
                        {align: 'center',field: 'engine', title: '类型'},
                        {align: 'center',field: 'data_length', title: '大小'},
                        {align: 'center',field: 'update_time', title: '更新时间'},
                        {align: 'center',field: 'rows', title: '行数'},
                        // {align: 'center', toolbar: '#operate', title: '操作', width: "10%"}
                    ]],
                    done: function(res, curr, count) {
                        $("table").width("100%");
                    },
                    reqData: function (res) {
                        console.log(res)
                    },
                });


            });

            //备份、优化、修复
            $(".backup,.repair,.optimize").on("click", function() {
                var that = $(this);
                var data = layui.table.checkStatus('searchReload').data;
                var tables = tableHandle(data, 'name'),
                    type = $(this).data("type");
                if(!tables) {
                    layer.msg("请选择操作项",{icon: 2, time: 1000, anim: 6});
                    return false;
                }

                layer.confirm('您确定要操作？', {
                    btn: ['确定','取消'] //按钮
                }, function(index){
                    $.ajax({
                        type: "POST",
                        data: {tables: tables, type: type},
                        dataType:"JSON",
                        url: "{{url('admin/databackup/read')}}",
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.msg('请勿关闭或刷新本页面，正在处理中...', {icon: 16, shade: 0.3, time: 0});
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    xadmin.close();
                                    xadmin.father_reload();
                                });
                            }else if(res.code == 0){
                                layer.msg(res.msg,{icon: 1, time: 1000, anim: 6});
                            }else{
                                layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                    layer.close(index);
                });
            })
        })
    </script>
    @include('admin.public.jsFile')
</body>
</html>
