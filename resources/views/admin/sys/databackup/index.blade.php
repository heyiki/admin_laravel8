<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    @include('admin.public.head')
</head>
<body>
    <div class="x-nav">
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
        </a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">
                        <button class="layui-btn" onclick="xadmin.open('备份数据库','{{url('admin/databackup/create')}}','85%','90%')">
                            <i class="layui-icon"></i>备份
                        </button>
                    </div>
                     <!-- 表格 -->
                    <div id="table" lay-filter="table"></div>
                    <!-- 表格操作列 -->
                    <script type="text/html" id="operate">
                        <a class="layui-btn layui-btn-xs" lay-event="import">还原</a>
                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="download">下载</a>
                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                    </script>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            layui.use(['table', 'form'], function () {
                var $ = layui.jquery,
                    table = layui.table,
                    form = layui.form;

                // 渲染表格
                var insTb = table.render({
                    elem: '#table',
                    id: "searchReload",
                    cellMinWidth: 80,
                    url: '{{url("admin/databackup/index")}}',
                    page: {
                        layout: ['prev', 'page', 'next', 'skip', 'count'],
                        groups: 5,
                    },
                    limit: {{$total}},
                    cols: [[
                        {align: 'center',field: 'filename', title: '备份名称'},
                        {align: 'center',field: 'part', title: 'part'},
                        {align: 'center',field: 'size', title: '大小'},
                        {align: 'center',field: 'compress', title: 'compress'},
                        {align: 'center',field: 'backtime', title: '时间'},
                        {align: 'center', toolbar: '#operate', title: '操作', width: "10%"}
                    ]],
                    done: function(res, curr, count) {
                        $("table").width("100%");
                    },
                    reqData: function (res) {
                        console.log(res)
                    },
                });

                //还原、下载、删除
                table.on('tool(table)', function(obj){
                    var filename = obj.data.time;
                    if(!filename){
                        layer.msg("系统繁忙，请重试",{icon: 2, time: 1000, anim: 6});
                        return false;
                    }
                    if(obj.event === 'del'){
                        layer.confirm('您确定要删除？', function(index){
                            $.post('{{url("admin/databackup/delFile")}}', {filename: filename}, function(res) {
                                if(res.code == 200){
                                    layer.msg(res.msg,{icon: 1, time: 1000, anim: 6},function(){
                                        obj.del();
                                    });
                                }else{
                                    layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
                                }
                            });
                            layer.close(index);
                        });
                    }else if(obj.event === 'import'){
                        layer.confirm('您确定要还原？', function(index){
                            ipmosrting(filename, null, null);
                            layer.close(index);
                        });
                    }else if(obj.event === 'download'){
                        layer.confirm('您确定要下载？', function(index){
                            layer.close(index);
                            location.href = "{{url('admin/databackup/downloadFile')}}?time="+filename;
                        });
                    }
                    return false;
                });
            });
        })
        //导入数据
        function ipmosrting(time, part = null, start = null) {
            var params = {
                time: time,
                part: part,
                start: start
            };
            $.ajax({
                type: "POST",
                data: params,
                dataType:"JSON",
                url: "{{url('admin/databackup/import')}}",
                beforeSend: function () {
                    // layer.msg('请勿关闭本页面，系统正在处理中', {icon: 16, shade: 0.3, time: 0});
                },
                success: function (res) {
                    console.log(res);
                    if(res.code == 200){
                        ipmosrting(time, res.data.part, res.data.start);
                        layer.msg(res.msg,{icon: 16, shade: 0.3, time: 0});
                    }else if(res.code == 0) {
                        layer.msg(res.msg,{icon: 1, time: 1000, anim: 6});
                    }else{
                        layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
                        return false;
                    }
                },
                complete: function () {
                    layer.closeAll('loading');
                },
                error: function (e) {
                    console.info("Error："+e);
                }
            });
        }
    </script>
    @include('admin.public.jsFile')
</body>
</html>
