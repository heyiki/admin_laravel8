<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    @include("admin.public.head")
</head>
<body class="login-bg">

    <div class="login layui-anim layui-anim-up">
        <div class="message">后台管理系统</div>
        <div id="darkbannerwrap"></div>
        <form method="post" class="layui-form" >
            <input name="account" placeholder="账号"  type="text" lay-verify="required" class="layui-input" autocomplete="off">
            <hr class="hr15">
            <input name="password" lay-verify="required" placeholder="密码"  type="password" class="layui-input" autocomplete="off">
            <hr class="hr15">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input name="captcha" placeholder="验证码"  type="text" lay-verify="required" class="layui-input" autocomplete="off">
                </div>
                <div class="layui-word-aux f5" style="float: right; ">
                    <img src="{{ captcha_src('flat') }}" title="点击图片切换验证码">
                </div>
            </div>
            <hr class="hr15">
            <input value="登录" lay-submit lay-filter="login" style="width:100%;" type="submit">
            <hr class="hr20" >
        </form>
        <div class="layui-form-item" style="text-align: center;">
            <a href="https://www.heyiki.top/" target="_blank" style="color: #999">Heyiki</a>
        </div>
    </div>

    <script>
        $(function  () {
            layui.use(['form'], function(){
                var form = layui.form;
                //监听提交
                form.on('submit(login)', function(data){
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: data.field,
                        dataType:"JSON",
                        url: "{{url('admin/signin')}}",
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function() {
                                    window.location.href = "{{url('admin/index')}}";
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:2000}, function() {
                                    f5();
                                });
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                });
            });
            $(".f5").on("click", function() {
                f5();
            })
        })
        function f5(){
            $(".f5").find("img").attr("src",'{{ captcha_src('flat') }}?'+Math.random());
        }
    </script>
</body>
</html>
