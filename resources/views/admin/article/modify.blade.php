<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 20px;">
                <form class="layui-form" name="myForm">
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            <span class="x-red">*</span>绑定分类
                        </label>
                        <div class="layui-input-inline" id="cat_id" style="width: 80%;">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            <span class="x-red">*</span>标题
                        </label>
                        <div class="layui-input-block ml0">
                          <input type="text" name="title" value="{{$model['title']}}" lay-verify="required" autocomplete="off" placeholder="请输入商品名称" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">描述</label>
                        <div class="layui-input-block ml0">
                          <textarea name="description" placeholder="请输入描述" class="layui-textarea">{{$model['description']}}</textarea>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            关键字
                        </label>
                        <div class="layui-input-block ml0">
                          <input type="text" name="keyword" value="{{$model['keyword']}}" autocomplete="off" placeholder="多个用英文状态下的逗号隔开" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            原文链接
                        </label>
                        <div class="layui-input-block ml0">
                          <input type="text" name="url" value="{{$model['url']}}" autocomplete="off" placeholder="原文链接" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">作者</label>
                            <div class="layui-input-inline">
                                <input type="text" name="author" value="{{$model['author']}}" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label">浏览量</label>
                            <div class="layui-input-inline">
                                <input type="number" name="click" value="{{$model['click'] ? : 0}}" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            缩略图
                        </label>
                        <div class="layui-input-block ml0">
                            <div class="fc-upload" id="picture">
                                @if($model['picture'])
                                    @foreach($model['picture'] as $v)
                                    <div class="fc-files">
                                        <img src="{{$v}}">
                                        <div class="fc-upload-cover">
                                            <i class="ivu-icon ivu-icon-ios-eye-outline"></i>
                                            <i class="ivu-icon ivu-icon-ios-trash-outline"></i>
                                        </div>
                                        <input type="hidden" name="picture[]" value="{{$v}}">
                                    </div>
                                    @endforeach
                                @endif
                                <div class="fc-upload-btn" onclick="xadmin.open('选择文件','{{url('admin/images/index')}}?catalog=article&field=picture&limit=1','60%','72%')">
                                    <i class="ivu-icon ivu-icon-images" style="font-size: 20px;"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">排序</label>
                            <div class="layui-input-inline">
                                <input type="text" name="sort" value="{{$model['sort'] ? : 50}}" autocomplete="off" class="layui-input" onkeyup="this.value=this.value.replace(/[^\d]/g,'')">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-inline">
                                <input type="radio" name="status" value="1" title="显示">
                                <input type="radio" name="status" value="0" title="隐藏">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label">热门</label>
                            <div class="layui-input-inline">
                                <input type="radio" name="is_hot" value="1" title="是">
                                <input type="radio" name="is_hot" value="0" title="否">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            <span class="x-red">*</span>内容
                        </label>
                        <div class="layui-input-block ml0">
                            <script id="details" name="details" type="text/plain">{{$model['details']}}</script>
                            <script type="text/javascript">UE.getEditor("details", {initialFrameWidth: '100%', initialFrameHeight: 550, autoHeightEnabled: false});</script>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <input type="hidden" name="id" value="{{$model['id']}}">
                        <label for="L_repass" class="layui-form-label"></label>
                        <button type="button" class="layui-btn" id="edit">提交</button>
                    </div>
                </form>
            </div>
        </div>

        <script>
            $(function(){
                $("input[type=radio][name=status][value='{{isShow($model['status'])}}']").attr("checked","checked");
                $("input[type=radio][name=is_hot][value='{{isShow($model['is_hot'])}}']").attr("checked","checked");

                layui.config({
                    base: '/static/admin/lib/'
                }).extend({
                    selectM: 'selectExtends/selectM',
                    selectN: 'selectExtends/selectN',
                }).use(['form', 'layer', 'jquery', 'selectN', 'selectM'], function() {
                    $ = layui.jquery;
                    var form = layui.form,
                    layer = layui.layer,
                    selectN = layui.selectN,
                    selectM = layui.selectM;

                    //加载分类
                    $.post("{{url('admin/article.article/cate')}}", {}, function(res) {
                        console.log(res)
                        if(res.code == 500) {
                            layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
                            return false;
                        }
                        var tagIns1 = selectM({
                            elem: '#cat_id',
                            data: res,
                            max: 2,
                            selected: [{{$cate_id}}],
                            verify:'required'
                        });
                    })

                    //监听提交
                    $("#edit").on("click",function(){
                        var that = $(this);
                        $.ajax({
                            type: "POST",
                            data: $("form[name=myForm]").serialize(),
                            dataType:"JSON",
                            url: "{{url('admin/article.article/edit')}}",
                            beforeSend: function () {
                                that.attr("disabled","disabled");
                                layer.load(2);
                            },
                            success: function (res) {
                                if(res.code == 200){
                                    layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                        xadmin.close();
                                        xadmin.father_reload();
                                    });
                                }else{
                                    layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                    return false;
                                }
                            },
                            complete: function () {
                                that.removeAttr("disabled");
                                layer.closeAll('loading');
                            },
                            error: function (e) {
                                console.info("Error："+e);
                            }
                        });
                    })
                });
            })
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
