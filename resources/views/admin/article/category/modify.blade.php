<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 20px;">
                <form class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            <span class="x-red">*</span>名称
                        </label>
                        <div class="layui-input-inline">
                          <input type="text" value="{{$model['cat_name']}}" name="cat_name"  autocomplete="off" placeholder="请输入名称" class="layui-input">
                        </div>
                        <div class="layui-form-mid layui-word-aux">
                            <span class="x-red">*</span>名称必填
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">父级ID</label>
                        <div class="layui-input-inline">
                            <select name="pid" lay-verify="required" lay-search="">
                                <option value="0">顶级</option>
                                @foreach($tree as $v)
                                <option value="{{$v['id']}}">{{$v['cat_name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="layui-form-mid layui-word-aux">
                            <span class="x-red">*</span>直接选择或搜索选择
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            <span>图标</span>
                        </label>
                        <div class="layui-input-block ml0">
                            <div class="fc-upload" id="picture">
                                @if($model['picture'])
                                    @foreach($model['picture'] as $v)
                                    <div class="fc-files">
                                        <img src="{{$v}}">
                                        <div class="fc-upload-cover">
                                            <i class="ivu-icon ivu-icon-ios-eye-outline"></i>
                                            <i class="ivu-icon ivu-icon-ios-trash-outline"></i>
                                        </div>
                                        <input type="hidden" name="picture[]" value="{{$v}}">
                                    </div>
                                    @endforeach
                                @endif
                                <div class="fc-upload-btn" onclick="xadmin.open('选择文件','{{url('admin/images/index')}}?catalog=category&field=picture&limit=1','60%','72%')">
                                    <i class="ivu-icon ivu-icon-images" style="font-size: 20px;"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-inline">
                          <input type="text" value="{{$model['sort'] ? : 50}}" name="sort" autocomplete="off" placeholder="请输入排序" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block ml0">
                            <input type="radio" name="is_show" value="1" title="显示">
                            <input type="radio" name="is_show" value="0" title="隐藏">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <input type="hidden" name="id" value="{{$model['id']}}">
                        <label for="L_repass" class="layui-form-label"></label>
                        <button class="layui-btn" lay-filter="edit" lay-submit="">提交</button>
                    </div>
                </form>
            </div>
        </div>

        <script>
            $("input[type=radio][name=is_show][value='{{isShow($model['is_show'])}}']").attr("checked","checked");
            $("select[name=pid] option[value='{{$model['pid']}}']").attr('selected','selected');

            layui.use(['form', 'layer', 'jquery'], function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

                //监听提交
                form.on('submit(edit)',function(data) {
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: data.field,
                        dataType:"JSON",
                        url: "{{url('admin/article.category/edit')}}",
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    xadmin.close();
                                    xadmin.father_reload();
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                });
            });
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
