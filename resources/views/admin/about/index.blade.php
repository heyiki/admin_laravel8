<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    @include('admin.public.head')
</head>
<body>
    <div class="x-nav">
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
        </a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row" style="margin-top: 10px;">
            <div class="layui-tab layui-tab-card">
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <form class="layui-form" name="myForm">
                            <div class="layui-form-item">
                                <label class="layui-form-label">
                                    <span class="x-red">*</span>内容
                                </label>
                                <div class="layui-input-block ml0">
                                    <script id="details" name="value" type="text/plain">{!! $model['value'] !!}</script>
                                    <script type="text/javascript">UE.getEditor("details", {initialFrameWidth: '100%', initialFrameHeight: 550, autoHeightEnabled: false});</script>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <input type="hidden" name="name" value="{{$type}}">
                                <input type="hidden" name="id" value="{{$id}}">
                                <label for="L_repass" class="layui-form-label"></label>
                                <button class="layui-btn" lay-filter="edit" lay-submit="">提交</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        layui.use(['layer', 'form'], function () {
            var $ = layui.jquery,
                layer = layui.layer,
                form = layui.form;

            //监听提交
            form.on('submit(edit)',function(data) {
                var that = $(this);
                $.ajax({
                    type: "POST",
                    data: $('form[name=myForm]').serialize(),
                    dataType:"JSON",
                    url: "{{url('admin/about/update')}}",
                    beforeSend: function () {
                        that.attr("disabled","disabled");
                        layer.load(2);
                    },
                    success: function (res) {
                        if(res.code == 200){
                            layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                window.location.href = "";
                            });
                        }else{
                            layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                            return false;
                        }
                    },
                    complete: function () {
                        that.removeAttr("disabled");
                        layer.closeAll('loading');
                    },
                    error: function (e) {
                        console.info("Error："+e);
                    }
                });
            })
        });
    </script>
    @include('admin.public.jsFile')
</body>
</html>
