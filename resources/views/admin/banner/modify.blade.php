<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    @include('admin.public.head')
</head>
<body>
    <div class="layui-fluid">
        <div class="layui-row" style="margin-top: 20px;">
            <form class="layui-form" name="myForm">
                <div class="layui-form-item">
                    <label class="layui-form-label">绑定位置</label>
                    <div class="layui-input-inline">
                        <select name="position" lay-verify="required" lay-search="">
                            <option value="">请选择</option>
                            @foreach($position as $v)
                            <option value="{{$v['id']}}">{{$v['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        <span class="x-red">*</span>直接选择或搜索选择
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">
                        <span class="x-red">*</span>轮播图
                    </label>
                    <div class="layui-input-block ml0">
                        <div class="fc-upload" id="picture">
                            @if($model['picture'])
                                @foreach($model['picture'] as $v)
                                <div class="fc-files">
                                    <img src="{{$v}}">
                                    <div class="fc-upload-cover">
                                        <i class="ivu-icon ivu-icon-ios-eye-outline"></i>
                                        <i class="ivu-icon ivu-icon-ios-trash-outline"></i>
                                    </div>
                                    <input type="hidden" name="picture[]" value="{{$v}}">
                                </div>
                                @endforeach
                            @endif
                            <div class="fc-upload-btn" onclick="xadmin.open('选择文件','{{url('admin/images/index')}}?catalog=banner&field=picture&limit=1','60%','72%')">
                                <i class="ivu-icon ivu-icon-images" style="font-size: 20px;"></i>
                            </div>
                        </div>
                        <span class="x-red">建议尺寸：345 * 550px</span>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">
                        <span class="x-red">*</span>名称
                    </label>
                    <div class="layui-input-block ml0">
                        <input type="text" name="name" value="{{$model['name']}}" lay-verify="required" autocomplete="off" placeholder="请输入名称" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">
                        链接
                    </label>
                    <div class="layui-input-block ml0">
                        <input type="text" name="url" value="{{$model['url']}}" lay-verify="required" autocomplete="off" placeholder="请输入url链接" class="layui-input">
                    </div>
{{--                    <div class="layui-form-mid layui-word-aux">--}}
{{--                        <span class="x-red">链接地址</span>--}}
{{--                    </div>--}}
                </div>
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-inline">
                            <input type="text" name="sort" value="{{$model['sort'] ? : 50}}" autocomplete="off" class="layui-input" onkeyup="this.value=this.value.replace(/[^\d]/g,'')">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-inline">
                            <input type="radio" name="is_show" value="1" title="显示">
                            <input type="radio" name="is_show" value="0" title="隐藏">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <input type="hidden" name="id" value="{{$model['id']}}">
                    <label for="L_repass" class="layui-form-label"></label>
                    <button type="button" class="layui-btn" id="edit">提交</button>
                </div>
            </form>
        </div>
    </div>

    <script>
        $(function(){
            $("input[type=radio][name=is_show][value='{{isShow($model['is_show'])}}']").attr("checked","checked");
            $("select[name=position] option[value='{{$model['position']}}']").attr('selected','selected');

            layui.config({
                base: '/static/admin/lib/'
            }).extend({
                selectM: 'selectExtends/selectM',
                selectN: 'selectExtends/selectN',
            }).use(['form', 'layer', 'jquery', 'selectN', 'selectM'], function() {
                $ = layui.jquery;
                var form = layui.form,
                    layer = layui.layer,
                    selectN = layui.selectN,
                    selectM = layui.selectM;

                //监听提交
                $("#edit").on("click",function(){
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: $("form[name=myForm]").serialize(),
                        dataType:"JSON",
                        url: "{{url('admin/banner/edit')}}",
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    xadmin.close();
                                    xadmin.father_reload();
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            });
        })
    </script>
    @include('admin.public.jsFile')
</body>
</html>
