<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5" id="searchForm">
                                <div class="layui-input-inline layui-show-xs-block">
                                    <select name="position" lay-verify="required" lay-search="">
                                        <option value="">全部</option>
                                        @foreach($position as $v)
                                        <option value="{{$v['id']}}">{{$v['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input type="text" name="name" placeholder="名称" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button type="button" class="layui-btn" id="search">
                                        <i class="layui-icon">&#xe615;</i>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-header">
                            <button class="layui-btn" onclick="xadmin.open('添加轮播图','{{url('admin/banner/create')}}','80%','90%')">
                                <i class="layui-icon"></i>添加
                            </button>
                        </div>
                        <!-- 表格 -->
                        <div id="table" lay-filter="table"></div>
                        <!-- 表格状态列 -->
                        <script type="text/html" id="is_show">
                            <input type="checkbox" lay-filter="status" lay-skin="switch" value="@{{d.id}}" data-table="banner" data-pk="id" data-field="is_show" lay-text="显示|隐藏" @{{d.is_show == 1 ? 'checked' : ''}}/>
                        </script>
                        <!-- 表格操作列 -->
                        <script type="text/html" id="operate">
                            <a class="layui-btn layui-btn-sm" onclick="xadmin.open('编辑轮播图','{{url('admin/banner/create')}}?id=@{{d.id}}','80%','90%')">编辑</a>
                            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function(){
                layui.use(['table', 'form'], function () {
                    var $ = layui.jquery,
                        table = layui.table,
                        form = layui.form;

                    // 渲染表格
                    var insTb = table.render({
                        elem: '#table',
                        id: "searchReload",
                        cellMinWidth: 80,
                        url: '{{url("admin/banner/index")}}',
                        page: {
                            layout: ['prev', 'page', 'next', 'skip', 'count'],
                            groups: 5,
                        },
                        limit: "{{$pageSize}}",
                        cols: [[
                            {align: 'center',field:'id', title: 'ID', sort: true, width: 80},
                            {
                                align: 'center',field: 'picture', title: '轮播图', templet: function (d) {
                                    return d.picture ? '<img src="'+ d.picture +'" width="80">' : '';
                                }
                            },
                            {align: 'center',field: 'name', title: '名称'},
                            {align: 'center',field: 'position', title: '位置'},
                            {align: 'center',field: 'url', title: 'url链接'},
                            {align: 'center',field: 'sort', title: '排序', edit: 'text'},
                            {align: 'center',templet: '#is_show', title: '状态'},
                            {align: 'center', toolbar: '#operate', title: '操作', width: "10%"}
                        ]],
                        done: function(res, curr, count) {
                            $("table").width("100%");
                        },
                        reqData: function (res) {
                            console.log(res)
                        },
                    });

                    //监听edit
                    table.on('edit(table)', function(obj){
                        var param = {
                            table: 'banner',
                            pk: 'id',
                            field: obj.field,
                            id: obj.data.id,
                            value: obj.value,
                        };
                        currEdit(param);
                    });

                    //删除
                    table.on('tool(table)', function(obj){
                        if(obj.event === 'del'){
                            var id = obj.data.id;
                            if(!id){
                                layer.msg("系统繁忙，请重试",{icon: 2, time: 1000, anim: 6});
                                return false;
                            }
                            layer.confirm('您确定要删除？', function(index){
                                $.post('{{url("admin/banner/delete")}}', {id: id}, function(res) {
                                    if(res.code == 200){
                                        layer.msg(res.msg,{icon: 1, time: 1000, anim: 6},function(){
                                            obj.del();
                                        });
                                    }else{
                                        layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
                                    }
                                });
                                layer.close(index);
                            });
                        }
                        return false;
                    });
                });
            })
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
