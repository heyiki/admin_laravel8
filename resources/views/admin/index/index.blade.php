<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include("admin.public.head")
    </head>
    <body class="index">
        <!-- 顶部开始 -->
        <div class="container">
            <div class="logo">
                <a href="javascript:;" title="后台管理系统">后台管理系统</a>
            </div>
            <div class="left_open">
                <a><i title="展开左侧栏" class="iconfont">&#xe699;</i></a>
            </div>
            <ul class="layui-nav right" lay-filter="">
                <li class="layui-nav-item">
                    <a href="javascript:;">{{$admin['account']}}</a>
                    <dl class="layui-nav-child">
                        <!-- 二级菜单 -->
                        <dd>
                            <a href="{{url('admin/logout')}}">退出</a>
                        </dd>
                    </dl>
                </li>
                <li class="layui-nav-item to-index">
                    <a href="/" target="_blank" rel="noreferrer">前台首页</a>
                </li>
            </ul>
        </div>
        <!-- 顶部结束 -->
        <!-- 中部开始 -->
        <!-- 左侧菜单开始 -->
        <div class="left-nav">
            <div id="side-nav">
                @include("admin.public.menu")
            </div>
        </div>
        <!-- <div class="x-slide_left"></div> -->
        <!-- 左侧菜单结束 -->
        <!-- 右侧主体开始 -->
        <div class="page-content">
            <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
                <ul class="layui-tab-title">
                    <li class="home">
                        <i class="layui-icon">&#xe68e;</i>我的桌面</li></ul>
                <div class="layui-unselect layui-form-select layui-form-selected" id="tab_right">
                    <dl>
                        <dd data-type="this">关闭当前</dd>
                        <dd data-type="other">关闭其它</dd>
                        <dd data-type="all">关闭全部</dd></dl>
                </div>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <iframe src='{{url("admin/welcome")}}' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
                    </div>
                </div>
                <div id="tab_show"></div>
            </div>
        </div>
        <div class="page-content-bg"></div>
        <style id="theme_style"></style>
        <!-- 右侧主体结束 -->
    </body>
    <script>
        $(function () {
            layui.config({
                base: '/static/admin/lib/layui/dist/'
            }).extend({
                notice: 'notice'
            });

            //layui模块的定义
            layui.use(['notice', 'jquery', 'layer'], function () {
                var notice = layui.notice;
                var layer = layui.layer;
                var $ = layui.jquery;

                // 初始化配置，同一样式只需要配置一次，非必须初始化，有默认配置
                notice.options = {
                    closeButton:true,//显示关闭按钮
                    debug:false,//启用debug
                    positionClass:"toast-top-right",//弹出的位置,
                    showDuration:"300",//显示的时间
                    hideDuration:"1000",//消失的时间
                    timeOut:"29000",//停留的时间
                    extendedTimeOut:"10000",//控制时间
                    showEasing:"swing",//显示时的动画缓冲方式
                    hideEasing:"linear",//消失时的动画缓冲方式
                    iconClass: 'toast-info', // 自定义图标，有内置，如不需要则传空 支持layui内置图标/自定义iconfont类名
                    onclick: null, // 点击关闭回调
                };

                // setInterval(show, 30000);

                function show(){
                    $.post('{:url("order.order/deliverRefund")}', function(res){
                        if(res.code == 200){
                            $.each(res.data, function(k, v){
                                if(v.status == 0){
                                    notice.info("待发货订单：" + v.order_sn);
                                }
                                if(v.refund_status == 1){
                                    notice.warning("待退款订单：" + v.order_sn);
                                }
                            });
                        }
                    });
                }
            });
        })
    </script>
</html>
