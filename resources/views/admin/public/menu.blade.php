<ul id="nav">
    @foreach ($menu as $v)
    <li>
        <a href="javascript:;">
            <i class="iconfont left-nav-li" lay-tips="{{ $v['menu_name'] }}">{!! $v['icon'] !!}</i>
            <cite>{{ $v['menu_name'] }}</cite>
            <i class="iconfont nav_right">&#xe697;</i>
        </a>
        <ul class="sub-menu">
            @foreach ($v['sub'] as $v1)
            <li>
                <a @if ($v1['sub']) href="javascript:;" @else onclick="xadmin.add_tab('{{$v1['menu_name']}}','/admin/{{$v1['controller']}}/{{$v1['action']}}')" @endif>
                    <i class="iconfont">{!! $v1['icon'] !!}</i>
                    <cite>{{$v1['menu_name']}}</cite>
                </a>
                @if ($v1['sub'])
                    <ul class="sub-menu">
                        @foreach ($v1['sub'] as $v2)
                        <li>
                            <a onclick="xadmin.add_tab('{{$v2['menu_name']}}','/admin/{{$v2['controller']}}/{{$v2['action']}}')">
                                <i class="iconfont">&#xe6a7;</i>
                                <cite>{{$v2['menu_name']}}</cite>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                @endif
            </li>
            @endforeach
        </ul>
    </li>
    @endforeach
</ul>
