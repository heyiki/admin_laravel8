<meta charset="UTF-8">
<title>后台管理系统</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="author" content="Heyiki,www.heyiki.top,1186933877@qq.com">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="shortcut icon" href="/static/admin/images/favicon.ico?v={{ time() }}">

<link rel="stylesheet" href="/static/admin/css/font.css">
<link rel="stylesheet" href="/static/admin/css/login.css">
<link rel="stylesheet" href="/static/admin/css/xadmin.css?v={{ time() }}">
<link rel="stylesheet" href="/static/admin/css/theme10.min.css">
<link rel="stylesheet" href="/static/admin/lib/layui/css/layui.css?v={{ time() }}">
<link rel="stylesheet" href="/static/admin/css/cp.min.css?v={{ time() }}">

<link rel="stylesheet" type="text/css" href="/static/admin/lib/upload/css/iview.css">
<link rel="stylesheet" type="text/css" href="/static/admin/lib/upload/css/upload.min.css">

<script type="text/javascript" charset="utf-8" src="/static/admin/js/jquery.min.js"></script>
<script type="text/javascript" charset="utf-8" src="/static/admin/lib/layui/layui.js"></script>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/xadmin.js"></script>

<script type="text/javascript" charset="utf-8" src="/static/plugins/UEditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="/static/plugins/UEditor/ueditor.all.js"></script>
<script type="text/javascript" charset="utf-8" src="/static/plugins/UEditor/lang/zh-cn/zh-cn.js"></script>

<script type="text/javascript" charset="utf-8" src="/static/plugins/Echarts/dist/echarts.min.js"></script>

<script type="text/javascript">
	// 是否开启刷新记忆tab功能
    var is_remember = false;
</script>
