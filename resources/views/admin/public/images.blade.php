<!DOCTYPE html>
<html lang="zh-CN" class="x-admin-sm">
<head>
    @include('admin.public.head')
</head>
<style>
    body{-moz-user-select:none;-webkit-user-select:none;-ms-user-select:none;user-select:none;}
    .layui-fluid{margin:0;padding: 0;}
    .layadmin-homepage-shadow{box-shadow: 0 1px 1px rgba(0,0,0,.05);background-color: #fff;border-radius: 0;border: 1px solid #e7ecf3;}
    .layui-tree-txt{cursor: pointer;}
    .clearfix:after{content:"";display:block;visibility:hidden;clear:both;height:0;}
    .image-box{padding-top: 10px}
    .image-box .image .layui-img-box{margin: 5px;border: 2px solid #ffffff;height: 100px;line-height: 100px;text-align: center;}
    .image-box .image .layui-img-box.on{border: 2px solid #5FB878;}
    .image-box .image .layui-img-box img{width: 90%;max-height:90%;vertical-align:middle;}
    .page .image_page{text-align: right;}
    .page .layui-box{text-align: left;}
    .layui-tree-txt.on{color:#1E9FFF}
    .line1{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width: 90%;}
    .layadmin-homepage-panel.left ::-webkit-scrollbar{width: 3px;height: auto;background-color: #ddd;}
    .layadmin-homepage-panel.left ::-webkit-scrollbar-thumb {border-radius: 1px;-webkit-box-shadow: inset 0 0 6px rgba(255,255,255,.3);background-color: #333;}
    .layadmin-homepage-panel.left ::-webkit-scrollbar-track {-webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.2);border-radius: 1px;background: #e5e5e5;}
    #app .layui-tree-btnGroup{color: #ffffff;padding: 3px 7px;position: absolute;top: -28px;left: 30px;background-color: #1E9FFF;}
    #app .layui-tree-btnGroup .layui-layer-TipsT{border-right-color: #1E9FFF;color: #ffffff}
    #app .layui-tree-iconClick{margin:0 0 0 9px;}
    @media screen and (min-width:1000px){
        .image-box .image .layui-img-box{
            height: 200px;
            line-height: 200px;
        }
    }
    .layui-bg-cyan{
        position: absolute;
        z-index: 98;
    }
</style>
<body style="background-color: #f2f2f2">

<div class="layui-fluid" id="app">
    <div class="layui-row">
        <!-- <div class="layui-col-md2 layui-col-xs2 layui-col-sm2">
            <div class="layadmin-homepage-panel layadmin-homepage-shadow left">
                <div class="layui-card text-center">
                    <div class="layui-card-header">
                        <div class="layui-unselect layui-form-select layui-form-selected">
                            <div class="layui-select-title">
                                <input type="text" name="title" placeholder="搜索分类" autocomplete="off" class="layui-input layui-input-search" style="height: 24px; line-height: 24px; padding-left: 7px; font-size: 12px; display: inline; padding-right: 0px; width: 100%;">
                            </div>
                        </div>
                    </div>
                    <div class="layui-card-body" style="padding: 0px; height: 455px; overflow: auto;">
                        <div class="demo-tree demo-tree-box">
                            <div class="layui-tree layui-tree-line">
                                <div class="layui-tree-set layui-tree-setHide">
                                    <div class="layui-tree-entry">
                                        <div class="layui-tree-main">
                                            <span class="layui-tree-iconClick">
                                                <i class="layui-icon">&nbsp;&nbsp;&nbsp;</i>
                                            </span>
                                            <span class="layui-tree-txt">全部图片</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-tree-set layui-tree-setHide">
                                    <div class="layui-tree-entry">
                                        <div class="layui-tree-main">
                                            <span class="layui-tree-iconClick">
                                                <i class="layui-icon ">&nbsp;&nbsp;&nbsp;</i>
                                            </span>
                                            <span class="layui-tree-txt line1 on">qq</span>
                                        </div>
                                        <div class="layui-btn-group layui-tree-btnGroup layui-layer layui-layer-tips" style="display: none;">
                                            <div>
                                                <i title="添加" class="layui-icon layui-icon-add-1"></i>
                                                <i title="编辑" class="layui-icon layui-icon-edit"></i>
                                                <i title="删除" class="layui-icon layui-icon-delete"></i>
                                            </div>
                                            <i class="layui-layer-TipsG layui-layer-TipsT"></i>
                                        </div>
                                    </div>
                                    <div class="layui-tree-pack layui-tree-lineExtend layui-tree-showLine" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="layui-col-md10 layui-col-xs10 layui-col-sm10" style="width: 100%;">
            <div class="layadmin-homepage-panel layadmin-homepage-shadow">
                <div class="layui-card text-center">
                    <div class="layui-card-header">
                        <div class="layadmin-homepage-pad-ver" style="text-align: left;">
                            <div class="layui-btn-group">
                                <!-- <button type="button" class="layui-btn layui-btn-normal layui-btn-sm">添加分类</button>  -->
                                <button type="button" class="layui-btn layui-btn-normal layui-btn-sm" id="fielUpload">上传图片</button>
                                <!-- <button type="button" class="layui-btn layui-btn-warm layui-btn-sm layui-btn-disabled">移动分类</button>  -->
                                <button type="button" class="layui-btn layui-btn-danger layui-btn-sm {$total == 0 ? 'layui-btn-disabled' : ''}" id="fielDelete">删除图片</button>
                                <div class="layui-input-inline">
                                    <select name="imgType" style="height: 30px;">
                                        <option value="1">压缩图</option>
                                        <option value="2">原图</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-card-body clearfix image-box" style="padding: 10px;height: 360px;z-index:10;">
                        @foreach($list as $v)
                            <div class="layui-col-md2 layui-col-xs2 layui-col-sm2 image">
                                <div class="layui-img-box" data-id="{{$v['id']}}">
                                    <img src="{{$model['imgtype'] == 1 ? $v['satt_dir'] : $v['att_dir']}}" title="{{$v['name']}}">
                                </div>
                            </div>
                        @endforeach
                        <!-- 暂无图片 -->
                        @if($total == 0)
                            <div class="layui-card-body clearfix image-box" style="padding: 10px; height: 360px; z-index: 10;">
                                <div class="empty-image" style="width: 100%; height: 100%; text-align: center;">
                                    <div class="layui-img-box"><img src="/static/admin/lib/upload/empty.jpg" alt="" style="height: 400px;"></div>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="layui-card-body clearfix page">
                        <div class="layui-col-md4 layui-col-xs4 layui-col-sm4">
                            <div class="layui-box" style="margin: 10px 0;">
                                <button type="button" class="layui-btn layui-btn-normal layui-btn-sm {{$total == 0 ? 'layui-btn-disabled' : ''}}" style="position: relative;z-index: 999;" id="fileUse">使用选中的图片</button>
                            </div>
                        </div>
                        <div class="layui-col-md8 layui-col-xs8 layui-col-sm8 image_page">
                            {!! $links->appends(['catalog'=>$model['catalog'],'limit'=>$model['limit'],'field'=>$model['field'],'imgtype'=>$model['imgtype']])->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        var item = 0,
            length = 0,
            v = 0, //计数初始值
            catalog = "{{$model['catalog']}}",
            limit = "{{$model['limit']}}",
            field = "{{$model['field']}}",
            imgtype = "{{$model['imgtype']}}"; //默认展示缩略图

        $("select[name=imgType] option[value="+imgtype+"]").attr('selected','selected');
        $("select[name=imgType]").change(function(){
            imgType = $(this).val();
            window.location.href = "{{url('admin/images/index')}}?catalog="+catalog+"&limit="+limit+"&field="+field+"&imgtype="+imgType+"&page={{$model['page']}}";
        })

        //图片选中和取消
        $(".image-box").on("click",".image",function(index){
            if($(this).find(".layui-img-box").hasClass("on")){
                var obj = $(".image-box .image");
                var index = $(this).index();
                length = obj.length;
                for (var i = 0; i < length; i++) {
                    if(obj.eq(i).find(".layui-img-box").hasClass("on")){
                        if(i == index){
                            v = obj.eq(i).find(".layui-bg-cyan").text();
                        }
                    }
                }
                for (var i = 0; i < length; i++) {
                    if(obj.eq(i).find(".layui-img-box").hasClass("on")){
                        var key = obj.eq(i).find(".layui-bg-cyan").text();
                        if(parseInt(key) > parseInt(v)){
                            var val = parseInt(key)-1;
                            obj.eq(i).find(".layui-bg-cyan").text(val);
                        }
                    }
                }
                $(this).find(".layui-img-box").removeClass("on");
                $(this).find(".layui-bg-cyan").remove();
                item--;
            }else{
                item++;
                $(this).find(".layui-img-box").addClass("on");
                $(this).prepend('<span class="layui-badge layui-bg-cyan">'+item+'</span>');
            }
        })
        //上传图片
        layui.use(['upload'], function(){
            var $ = layui.jquery,
                upload = layui.upload;
            //多图片上传
            upload.render({
                elem: '#fielUpload',
                url: '{{url("admin/images/create")}}',
                multiple: true,
                data: {catalog: catalog},
                before: function () {
                    layer.load(2);
                },
                done: function(res){
                    // console.log(res)
                    layer.closeAll('loading');
                    if(res.code == 200){
                        layer.msg(res.msg, {icon: 1, shift:6, time:1000}, function(){
                            window.location.reload();
                        });
                    }else{
                        layer.msg(res.msg, {icon: 5, shift:6, time:1000});
                        return false;
                    }
                },
                error: function(){

                }
            });
        });

        //使用选中图片，按照序号返回
        $("#fileUse").on("click",function(){
            var obj = $(".image-box .image"),
                length = obj.length,
                list = new Array(),
                index = parent.layer.getFrameIndex(window.name); //获取窗口索引;
            for (var i = 0; i < length; i++) {
                var id = "",key = "";
                if(obj.eq(i).find(".layui-img-box").hasClass("on")){
                    id = obj.eq(i).find(".layui-img-box").attr("data-id");
                    key = obj.eq(i).find(".layui-bg-cyan").text();
                    list.push([id,key]);
                }
            }
            if(list.length == 0){
                layer.msg('请选择使用图片', {icon: 2, shift:6, time: 2000, });
                return false;
            }else if(parseInt(list.length) > parseInt(limit)){
                layer.msg('最多只能选择 '+limit+' 张', {icon: 2, shift:6, time: 2000, });
                return false;
            }else{
                var leng = parent.$('#'+field).find(".fc-files").length;
                if(parseInt(leng * 1 + list.length) > parseInt(limit)){
                    layer.msg('最多只能选择 '+limit+' 张', {icon: 2, shift:6, time: 2000, });
                    return false;
                }
            }

            //请求参数
            var html = "";
            $.ajax({
                type: "POST",
                data: {data: JSON.stringify(list)},
                dataType:"JSON",
                url: "{{url('admin/images/read')}}",
                beforeSend: function () {
                    layer.load(2);
                },
                success: function (res) {
                    // console.log(res)
                    if(res.code == 200){
                        $.each(res.data.list, function(k, v){
                            var src = imgtype == 1 ? v.satt_dir : v.att_dir;
                            html += '<div class="fc-files">'+
                                        '<img src="'+ src +'">'+
                                        '<div class="fc-upload-cover">'+
                                            '<i class="ivu-icon ivu-icon-ios-eye-outline"></i>'+
                                            '<i class="ivu-icon ivu-icon-ios-trash-outline"></i>'+
                                        '</div>'+
                                        '<input type="hidden" name="'+field+'[]" value="'+ src +'">'+
                                    '</div>';
                        });
                        parent.$('#'+field).prepend(html);
                        parent.layer.close(index);
                    }else{
                        layer.msg(res.msg, {icon: 2, shift:6, time:1000});
                        return false;
                    }
                },
                complete: function () {
                    layer.closeAll('loading');
                },
                error: function (e) {
                    console.info("Error："+e);
                }
            });
        })

        //删除图片
        $("#fielDelete").on("click",function(){
            var obj = $(".image-box .image"),
                length = obj.length,
                ids = "";
            layer.confirm('您确定删除选中图片？', {
                btn: ['确定','取消']
            }, function(){
                for (var i = 0; i < length; i++) {
                    if(obj.eq(i).find(".layui-img-box").hasClass("on")){
                        ids += ","+obj.eq(i).find(".layui-img-box").attr("data-id");
                    }
                }
                if(!ids) {
                    layer.msg('请选择删除图片', {icon: 2, shift:6, time: 1500, });
                    return false;
                }
                $.ajax({
                    type: "POST",
                    data: {ids: ids},
                    dataType:"JSON",
                    url: "{{url('admin/images/delete')}}",
                    beforeSend: function () {
                        layer.load(2);
                    },
                    success: function (res) {
                        if(res.code == 200){
                            layer.msg(res.msg, {icon: 1, shift:6, time:1000}, function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(res.msg, {icon: 2, shift:6, time:1000});
                            return false;
                        }
                    },
                    complete: function () {
                        layer.closeAll('loading');
                    },
                    error: function (e) {
                        console.info("Error："+e);
                    }
                });
            });
        })
    })
</script>
</body>
</html>
