<script type="text/javascript">
	$(function(){
		layui.use(['layer', 'util', 'table', 'form'], function () {
	        var $ = layui.jquery,
	        	layer = layui.layer,
	        	util = layui.util,
	        	table = layui.table,
	        	form = layui.form;
            ////监听单元格动作
            form.on('switch(status)', function(obj){
                var param = {
                    table: $(this).attr("data-table"),
                    pk: $(this).attr("data-pk"),
                    field: $(this).attr("data-field"),
                    id: obj.value,
                    checked: obj.elem.checked,
                };
                currEdit(param);
            });

            ////监听单元格动作
            form.on('switch(is_hot)', function(obj){
                var param = {
                    table: $(this).attr("data-table"),
                    pk: $(this).attr("data-pk"),
                    field: $(this).attr("data-field"),
                    id: obj.value,
                    checked: obj.elem.checked,
                };
                currEdit(param);
            });

            //监听sku指定开关
            form.on('switch(skuStatus)', function(data){
                layer.msg('开关checked：'+ (this.checked ? 'true' : 'false'), {
                    offset: '6px'
                });
                layer.tips('温馨提示：请注意开关状态的文字可以随意定义，而不仅仅是ON|OFF', data.othis)
            });

			//搜索
	        $("#search").on("click",function(){
	            var where = formJson("searchForm");
	            table.reload('searchReload', {
	                where: where,
	                page: {
	                    curr: 1
	                }
	            });
	        })
	    });
		//查看图片
		$("body").on("click",'.ivu-icon-ios-eye-outline',function(){
			var src = $(this).parent().prev("img").attr("src");
			var ivu =  '<div data-transfer="true" class="v-transfer-dom">'+
				            '<div class="ivu-modal-mask"></div>'+
				            '<div class="ivu-modal-wrap">'+
				                '<div class="ivu-modal" style="width: 520px;">'+
				                    '<div class="ivu-modal-content">'+
				                        '<a class="ivu-modal-close">'+
				                            '<i class="ivu-icon ivu-icon-ios-close-empty"></i>'+
				                        '</a>'+
				                        '<div class="ivu-modal-header">'+
				                            '<div class="ivu-modal-header-inner">预览</div>'+
				                        '</div>'+
				                        '<div class="ivu-modal-body">'+
				                            '<img src="'+ src +'" style="width: 100%;">'+
				                        '</div>'+
				                    '</div>'+
				                '</div>'+
				            '</div>'+
				        '</div>';
			$("body").append(ivu);
		})
		//关闭预览
		$("body").on("click",".ivu-icon-ios-close-empty",function(){
			$("body").find(".v-transfer-dom").remove();
		})
		//删除图片
		$("body").on("click",'.ivu-icon-ios-trash-outline',function(){
			$(this).parent().parent().remove();
		})

		//批量删除
        $("body").on("click", ".delete", function() {
            var data = layui.table.checkStatus('searchReload').data;
            var ids = tableHandle(data, 'id');
            var url = $(this).data(url);
            if(ids){
                layer.confirm('您确定要删除？', function(){
                    $.post(url, {id: ids}, function(res) {
                        if(res.code == 200){
                            layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
                        }
                    });
                });
                return false;
            }
            layer.msg("请选择操作项",{icon: 2, time: 1000, anim: 6});
        })

	})

	/**
	 * 表单序列化成json字符串的方法
	 * @param  {[type]} formId 表单ID
	 * @return {[type]}        [description]
	 */
    function formJson(formId) {
        var arr = $('#' + formId).serializeArray();
        /*请求参数转json对象*/
        var json = {};
        $(arr).each(function(){
            json[this.name] = this.value;
        });
        return json;
    }

    /**
     * 获取table的checkbox选中值
     * @param  {[type]} data  layui.table.checkStatus('searchReload').data;
     * @param  {String} field [description]
     * @return {[type]}       [description]
     */
    function tableHandle(data, field = 'id'){
    	if(data.length <= 0) return false;
    	var ids = "";
    	$(data).each(function(k, v){
    		if(v[field]){
    			ids += "," + v[field];
    		}
    	})
    	return ids;
    }

    /**
     * 统一修改
     * @param  {[type]} param [description]
     * var param = {
	            table: '', 表名
	            pk: '',  主键
	            field: '', 字段
	            id: '',  id值
	            value: '',  //修改值
	        };
     * @return {[type]}       [description]
     */
    function currEdit(param) {
    	$.post('{{url("admin/index/currEdit")}}', param, function(res) {
	        if(res.code == 200){
	            layer.msg(res.msg,{icon: 1, time: 1000, anim: 6});
	        }else{
	            layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
	        }
	    });
    }
</script>
