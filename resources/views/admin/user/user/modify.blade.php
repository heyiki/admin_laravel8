<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 20px;">
                <form class="layui-form" name="myForm">
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            姓名
                        </label>
                        <div class="layui-input-inline">
                          <input type="text" value="{{$model['real_name']}}" name="real_name"  autocomplete="off" placeholder="请输入姓名" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            性别
                        </label>
                        <div class="layui-input-inline">
                            <input type="radio" name="sex" value="0" title="保密">
                            <input type="radio" name="sex" value="1" title="男">
                            <input type="radio" name="sex" value="2" title="女">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            <span class="x-red">*</span>手机号
                        </label>
                        <div class="layui-input-inline">
                          <input type="text" value="{{$model['mobile']}}" name="mobile"  autocomplete="off" placeholder="请输入姓名" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            生日
                        </label>
                        <div class="layui-input-inline">
                          <input type="text" value="{{$model['birthday']}}" name="birthday" id="birthday" autocomplete="off" placeholder="请输入生日" class="layui-input">
                        </div>
                    </div>
                    <!-- <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block ml0">
                            <input type="radio" name="is_show" value="1" title="启用">
                            <input type="radio" name="is_show" value="0" title="禁用">
                        </div>
                    </div> -->
                    <div class="layui-form-item">
                        <input type="hidden" name="uid" value="{{$model['uid']}}">
                        <label for="L_repass" class="layui-form-label"></label>
                        <button type="button" class="layui-btn" id="edit">提交</button>
                    </div>
                </form>
            </div>
        </div>
        <script>
            $(function(){
                $("input[type=radio][name=is_show][value='{{isShow($model['is_show'])}}']").attr("checked","checked");
                $("input[type=radio][name=sex][value='{{isShow($model['sex'])}}']").attr("checked","checked");
                $("select[name=spread_uid] option[value='{{$model['spread_uid']}}']").attr('selected','selected');

                layui.use(['form', 'layer', 'jquery', 'laydate'], function() {
                    $ = layui.jquery;
                    var form = layui.form,
                    layer = layui.layer,
                    laydate = layui.laydate;

                    laydate.render({
                        elem: "#birthday",
                    });
                });
                //监听提交
                $("#edit").on("click",function(){
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: $("form[name=myForm]").serialize(),
                        dataType:"JSON",
                        url: "{{url('admin/user/edit')}}",
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            // console.log(res)
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    xadmin.close();
                                    xadmin.father_reload();
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            })
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
