<!DOCTYPE html>
<html class="x-admin-sm">
<head>
    @include('admin.public.head')
</head>
<body>
    <div class="x-nav">
        <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
        </a>
    </div>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body ">
                        <form class="layui-form layui-col-space5" id="searchForm">
                            <input type="hidden" name="ids" value="">
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="uid"  placeholder="UID" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="real_name"  placeholder="姓名" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="nickname"  placeholder="昵称" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-inline layui-show-xs-block">
                                <input type="text" name="mobile"  placeholder="手机号" autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-input-inline layui-show-xs-block">
                                <button type="button" class="layui-btn" id="search">
                                    <i class="layui-icon">&#xe615;</i>
                                </button>
                                <button type="button" class="layui-btn layui-btn-warm layui-btn-sm export">
                                    <i class="fa fa-floppy-o" style="margin-right: 3px;"></i>导出
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="layui-card-header">
                        <button class="layui-btn" onclick="xadmin.open('添加会员','{{url('admin/user/create')}}','70%','80%')">
                            <i class="layui-icon"></i>添加
                        </button>
                    </div>
                    <!-- 表格 -->
                    <div id="table" lay-filter="table"></div>
                    <!-- 表格状态列 -->
                    {{--<script type="text/html" id="status">
                        <input type="checkbox" lay-filter="status" lay-skin="switch" value="@{{d.uid}}" data-table="user" data-pk="uid" data-field="is_show" lay-text="启用|禁用" @{{d.is_show == 1 ? 'checked' : ''}}/>
                    </script>--}}
                    <!-- 表格操作列 -->
                    <script type="text/html" id="operate">
                        <div class="ltc">
                            <a class="layui-btn layui-btn-sm" onclick="xadmin.open('编辑会员','{{url('admin/user/create')}}?uid=@{{d.uid}}','70%','80%')">编辑</a>
                            <a class="layui-btn layui-btn-primary layui-btn-sm" onclick="xadmin.open('查看详情','{{url('admin/user/read')}}?uid=@{{d.uid}}','85%','90%')">详情</a>
                            <a class="layui-btn layui-btn-normal layui-btn-xs" onclick="xadmin.open('余额积分','{{url('admin/user/balance')}}?uid=@{{d.uid}}','70%','80%')">余额</a>
                            {{--<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>--}}
                        </div>
                    </script>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            layui.use(['table', 'form', 'laydate'], function () {
                var $ = layui.jquery,
                    table = layui.table,
                    laydate = layui.laydate,
                    form = layui.form;
                //日期
                $(".laydate").each(function(){
                    laydate.render({
                        elem: this,
                    });
                })

                // 渲染表格
                var insTb = table.render({
                    elem: '#table',
                    id: "searchReload",
                    cellMinWidth: 80,
                    url: '{{url("admin/user/index")}}',
                    page: {
                        layout: ['prev', 'page', 'next', 'skip', 'count'],
                        groups: 5,
                    },
                    limit: "{{$pageSize}}",
                    cols: [[
                        {type:'checkbox', width: 80},
                        // {align: 'center',field:'uid', title: 'UID', sort: true, width: '10%'},
                        {
                            align: 'center', field: 'avatar', title: '头像', width: '10%', templet: function (d) {
                                return '<img src="'+ (d.avatar ? d.avatar : '') +'" width="80">';
                            }
                        },
                        {
                            align: 'center', field: 'nickname', title: '个人信息', width: '15%' ,templet: function (d) {
                                return '<div class="layui-table-cell" style="text-align: left;margin-left: 10px;">'+
                                            '<p>UID：'+d.uid+'</p>'+
                                            '<p>姓名：'+d.real_name+'</p>'+
                                            '<p>昵称：'+d.nickname+'</p>'+
                                            '<p>性别：'+d.sex+'</p>'+
                                            '<p>手机号：'+d.mobile+'</p>'+
                                            '<p>邀请码：'+d.invite_code+'</p>'+
                                            '<p>来源：'+d.user_type+'</p>'+
                                        '</div>';
                            }
                        },
                        {align: 'center',field: 'user_balance', title: '余额'},
                        {align: 'center',field: 'integral', title: '积分'},
                        {align: 'center',field: 'pay_count', title: '购买次数'},
                        {
                            align: 'center',field: 'name', title: '访问日期', width: '15%' ,templet: function (d) {
                                return '<div class="layui-table-cell">'+
                                            '<p>首次：' + d.create_time + '</p>'+
                                            '<p>最近：' + d.last_time + '</p>'+
                                            '<p>登录设备：'+d.login_type+'</p>'+
                                        '</div>';
                            }
                        },
                        {align: 'center', toolbar: '#operate', title: '操作', width: "15%"}
                    ]],
                    done: function(res, curr, count) {
                        $("table").width("100%");
                    },
                    reqData: function (res) {
                        console.log(res)
                    },
                });

                //删除
                table.on('tool(table)', function(obj){
                    if(obj.event === 'del'){
                        var id = obj.data.uid;
                        if(!id){
                            layer.msg("系统繁忙，请重试",{icon: 2, time: 1000, anim: 6});
                            return false;
                        }
                        layer.confirm('您确定要删除与会员一切相关数据？删除后无法恢复！', function(index){
                            $.post('{{url("admin/user/delete")}}', {uid: id}, function(res) {
                                if(res.code == 200){
                                    layer.msg(res.msg,{icon: 1, time: 1000, anim: 6},function(){
                                        obj.del();
                                    });
                                }else{
                                    layer.msg(res.msg,{icon: 2, time: 1000, anim: 6});
                                }
                            });
                            layer.close(index);
                        });
                    }
                });
            });

            /* 导出 */
            $("body").on("click",'.export',function(){
                var data = layui.table.checkStatus('searchReload').data;
                var ids = tableHandle(data, 'uid');
                if(ids){
                    $('input[name=ids]').val(ids);
                }else{
                    $('input[name=ids]').val('');
                }
                layer.confirm('您确定要导出？', function(){
                    $.ajax({
                        type: "POST",
                        data: $("form[id=searchForm]").serialize(),  // 通过serialize()方法序列化表单值
                        dataType:"JSON",
                        url: "{{url('admin/user/exportExcel')}}",
                        beforeSend: function () {
                            $(this).attr("disabled","disabled");
                            layer.load(1);
                        },
                        success: function (res) {
                            if(res.code == 500) {
                                layer.msg(res.msg, {icon: 2});
                                return false;
                            }
                            if(res !== false){
                                layer.msg('导出成功，是否下载文件？', {
                                    time: 60000,
                                    btn: ['下载','取消'],
                                    success: function(layero){
                                        var btn = layero.find('.layui-layer-btn');
                                        btn.find('.layui-layer-btn0').attr({
                                            href: res.data.src,
                                            download: res.data.time
                                        });
                                    }
                                });
                            }else{
                                layer.msg('导出失败', {icon: 2});
                            }
                        },
                        complete: function () {
                            $(this).removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info(e);
                        }
                    });
                });
            });
        })
    </script>
    @include('admin.public.jsFile')
</body>
</html>
