<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row" style="margin-top: 10px;">
                <form class="layui-form" name="myForm">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header" style="font-size: 16px;">{{$model['nickname']}} / {{$model['mobile']}}</div>
                            <div class="layui-card-body ">
                                <ul class="layui-row layui-col-space10 layui-this x-admin-carousel x-admin-backlog">
                                    <li class="layui-col-md2 layui-col-xs6">
                                        <a href="javascript:;" class="x-admin-backlog-body">
                                            <h3>余额</h3>
                                            <p>
                                                <cite>{{$model['user_balance']}}</cite>
                                            </p>
                                        </a>
                                    </li>
                                    <li class="layui-col-md2 layui-col-xs6">
                                        <a href="javascript:;" class="x-admin-backlog-body">
                                            <h3>积分</h3>
                                            <p>
                                                <cite>{{$model['integral']}}</cite>
                                            </p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">余额操作</label>
                        <div class="layui-input-block ml0">
                            <input type="radio" name="is_balance" value="1" title="增加" checked="">
                            <input type="radio" name="is_balance" value="2" title="扣除">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            余额
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" name="user_balance" autocomplete="off" class="layui-input" onkeyup="this.value=this.value.replace(/^(\d*\.?\d{0,2}).*/,'$1')">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">积分操作</label>
                        <div class="layui-input-block ml0">
                            <input type="radio" name="is_integral" value="1" title="增加" checked="">
                            <input type="radio" name="is_integral" value="2" title="扣除">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">
                            积分
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" name="integral" autocomplete="off" class="layui-input" onkeyup="this.value=this.value.replace(/^(\d*\.?\d{0,2}).*/,'$1')">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <input type="hidden" name="uid" value="{{$model['uid']}}">
                        <label for="L_repass" class="layui-form-label"></label>
                        <button type="button" class="layui-btn" id="edit">提交</button>
                    </div>
                </form>
            </div>
        </div>
        <script>
            $(function(){
                //监听提交
                $("#edit").on("click",function(){
                    var that = $(this);
                    $.ajax({
                        type: "POST",
                        data: $("form[name=myForm]").serialize(),
                        dataType:"JSON",
                        url: "{{url('admin/user/update_balance')}}",
                        beforeSend: function () {
                            that.attr("disabled","disabled");
                            layer.load(2);
                        },
                        success: function (res) {
                            // console.log(res)
                            if(res.code == 200){
                                layer.msg(res.msg, {icon: 1,shift:6,time:1000}, function(){
                                    window.location.reload();
                                });
                            }else{
                                layer.msg(res.msg, {icon: 5,shift:6,time:1000});
                                return false;
                            }
                        },
                        complete: function () {
                            that.removeAttr("disabled");
                            layer.closeAll('loading');
                        },
                        error: function (e) {
                            console.info("Error："+e);
                        }
                    });
                })
            })
        </script>
        @include('admin.public.jsFile')
    </body>
</html>
