<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        @include('admin.public.head')
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12 layui-col-sm12 layui-col-lg12">
                    <div class="layui-card">
                        <div class="layui-card-header">会员详情</div>
                        <div class="layui-card-body">
                            <ul class="layui-row layui-col-space10 layui-this">
                                <li class="layui-col-xs2">
                                    <div class="backlog-body">
                                        <h3>手机号码</h3>
                                        <p><cite>{{$model['mobile']}}</cite></p>
                                    </div>
                                </li>
                                <li class="layui-col-xs2">
                                    <div class="backlog-body">
                                        <h3>昵称</h3>
                                        <p><cite>{{$model['nickname']}}</cite></p>
                                    </div>
                                </li>
                                <li class="layui-col-xs2">
                                    <div class="backlog-body">
                                        <h3>账户余额</h3>
                                        <p><cite>{{$model['user_balance']}}</cite></p>
                                    </div>
                                </li>
                                <li class="layui-col-xs2">
                                    <div class="backlog-body">
                                        <h3>积分</h3>
                                        <p><cite>{{$model['integral']}}</cite></p>
                                    </div>
                                </li>
                                <li class="layui-col-xs2" style="width: 100%;">
                                    <div class="backlog-body">
                                        <h3>默认收货地址</h3>
                                        <p><cite>{{$model['address']}}</cite></p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="layui-col-md12 layui-col-sm12 layui-col-lg12">
                <div class="layui-card">
                    <div class="layui-card-header">其他记录</div>
                    <div class="layui-card-body">
                        <div class="layui-tab layui-tab-card">
                            <ul class="layui-tab-title">
                                <li class="layui-this">余额明细</li>
                                <li class="">积分明细</li>
                            </ul>
                            <div id="content" class="layui-tab-content">
                                <div class="layui-tab-item layui-show">
                                    <div id="balance" lay-filter="table"></div>
                                </div>
                                <div class="layui-tab-item">
                                    <div id="integral" lay-filter="table"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </body>
</html>
<script>
    $(function(){
        layui.use(['table'], function() {
            $ = layui.jquery;
            var table = layui.table;
            //余额明细
            table.render({
                elem: '#balance',
                cellMinWidth: 80,
                url: '{{url("admin/user/balanceInfo")}}',
                where: {uid: {{$model['uid']}}},
                page: {
                    layout: ['prev', 'page', 'next', 'skip', 'count'],
                    groups: 5,
                },
                limit: "{{$pageSize}}",
                cols: [[
                    {align: 'center',field: 'title', title: '来源/用途'},
                    {align: 'center',field: 'status', title: '状态'},
                    {align: 'center',field: 'balance', title: '变动前'},
                    {align: 'center',field: 'number', title: '金额变动'},
                    {align: 'center',field: 'create_time', title: '日期'},
                    {align: 'center',field: 'mark', title: '备注'},
                ]],
                done: function(res, curr, count) {
                    $("table").width("100%");
                },
                reqData: function (res) {
                    console.log(res)
                },
            });
            //积分明细
            table.render({
                elem: '#integral',
                cellMinWidth: 80,
                url: '{{url("admin/user/integralInfo")}}',
                where: {uid: {{$model['uid']}}},
                page: {
                    layout: ['prev', 'page', 'next', 'skip', 'count'],
                    groups: 5,
                },
                limit: "{{$pageSize}}",
                cols: [[
                    {align: 'center',field: 'title', title: '来源/用途'},
                    {align: 'center',field: 'status', title: '状态'},
                    {align: 'center',field: 'balance', title: '变动前'},
                    {align: 'center',field: 'number', title: '积分变动'},
                    {align: 'center',field: 'create_time', title: '日期'},
                    {align: 'center',field: 'mark', title: '备注'},
                ]],
                done: function(res, curr, count) {
                    $("table").width("100%");
                },
                reqData: function (res) {
                    console.log(res)
                },
            });
        });
    })
</script>
