<?php
namespace App\Validates\Admin;

use App\Validates\BaseValidate;

class UserValidate extends BaseValidate
{
	protected $rule =   [
        'mobile' =>  ['required','mobile'],
    ];

    protected $message  =   [
        'mobile.required'   => '请输入手机号',
        'mobile.mobile'     => '手机号格式不正确',
    ];

    protected $scene = [
        'edit' => ['mobile'],
    ];
}
