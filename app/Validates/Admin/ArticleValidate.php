<?php
namespace App\Validates\Admin;

use App\Validates\BaseValidate;

class ArticleValidate extends BaseValidate
{
	protected $rule =   [
        'cat_name'  => ['required'],
        'cat_id'    => ['required'],
        //文章
        'title'     => ['required'],
        'details'   => ['required'],
    ];

    protected $message  =   [
        'cat_name.required' => '分类名称不能为空',
        'cat_id.required'   => '请选择绑定分类',
        //文章
        'title.required'    => '标题不能为空',
        'details.required'  => '内容不能为空',
    ];

    protected $scene = [
        'category'  =>  ['cat_name'],
        'article'   =>  ['cat_id','title','details'],
    ];
}
