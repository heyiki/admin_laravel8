<?php
namespace App\Validates\Admin;

use App\Validates\BaseValidate;

class SystemValidate extends BaseValidate {

    protected $rule = [
        'menu_name'     => ['required'],
        'module'        => ['required'],
        'controller'    => ['required'],
        'action'        => ['required'],
        //角色管理
        'title'         => ['required'],
        //管理员
        'real_name'     => ['required'],
        'account'       => ['required'],
        'roles_id'      => ['required'],
        //系统消息
        'content'       => ['required'],
    ];

    protected $message = [
        'menu_name.required'     => '名称不能为空',
        'module.required'        => '应用名称不能为空',
        'controller.required'    => '控制器名称不能为空',
        'action.required'        => '方法名称不能为空',
        'title.required'         => '名称不能为空',
        'account.required'       => '账号不能为空',
        'real_name.required'     => '姓名不能为空',
        'roles_id.required'      => '请选择角色',
        'password.required'      => '密码不能为空',
        'captcha.required'       => '验证码错误',
        'content.required'       => '内容不能为空',
    ];

    protected $scene = [
        'menu'      =>  ['menu_name', 'controller', 'action'],
        'roles'     => ['title'],
        'admin'     => ['account', 'real_name', 'roles_id'],
        'message'   => ['content'],
    ];
}
