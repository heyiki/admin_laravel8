<?php
namespace App\Validates\Admin;

use App\Validates\BaseValidate;

class BannerValidate extends BaseValidate {

    protected $rule = [
        'position'  => ['required'],
//        'picture'   => ['required'],
        'name'      => ['required'],
    ];

    protected $message = [
        'position.required'  => '请选择轮播位置',
        'picture.required'   => '请上传图片',
        'name.required'      => '名称不能为空',
    ];

}
