<?php
namespace App\Validates\Admin;

use App\Validates\BaseValidate;

/**
 * 后台登录验证器
 */
class LoginValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'account'   =>  ['required'],
        'password'  =>  ['required'],
        'captcha'   => ['required', 'captcha'],
    ];
    //自定义验证信息
    protected $message = [
        'account.required'  =>  '请输入账号',
        'password.required' =>  '请输入密码',
        'captcha.required'   =>  '请输入验证码',
        'captcha.captcha'   =>  '验证码错误',
    ];

    //自定义场景
    protected $scene = [
        'login' => ['account','password','captcha'],
    ];
}
