<?php
namespace App\Utils;

class FfmpegUtil
{
    /**
     * 截取视频图片、获得视频文件的总长度时间和创建时间
     * $path 视频路径
     * $width 截图大小  宽
     * $height 截图大小  长
     * $second 第几秒截图，生成几秒gif图
     * $type 转换类型 gif png
     * linux 安装ffmpeg教程 https://www.linuxidc.com/Linux/2018-10/154934.htm
     */
    public static function ffmpegPicture($path, $width = '', $height = '', $second = 1, $type = "gif", $catalog = 'video_cover'){
        try{
            if($path){
                ini_set('memory_limit','1024M'); //扩大内存
                $url = str_replace($_SERVER['HTTP_ORIGIN'],public_path(),$path);//获取视频绝对路径
                $name = md5((string) microtime(true)).'.'.$type;//图片名字
                $save_path = public_path('uploads/'. $catalog . DIRECTORY_SEPARATOR);
                if(!file_exists($save_path)) {
                    mkdir($save_path,0777,TRUE);
                    chmod($save_path,0777);
                }
                $vtime = exec("ffmpeg -i ".$url." 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//");//总长度
                if(HisToS($vtime) <= 3){
                    unlink($url);//删除文件
                    return ['code'=>500, 'msg'=>'视频时间过短'];
                }
                //视频大于5M进行压缩
                if(filesize($url) > 1024*1024*5 && $catalog){
                    $thunb_url = public_path('uploads/'. $catalog . DIRECTORY_SEPARATOR . date('Ymd') . DIRECTORY_SEPARATOR . md5((string) microtime(true)).'.mp4');
                    exec("ffmpeg -i ".$url." -vcodec libx264 -crf 20 ".$thunb_url);
                    unlink($url);//删除原文件
                    $url = $thunb_url;
                }
                switch ($type) {
                    case 'png':
                        if($width && $height){
                            //ffmpeg命令,具体啥意思可以百度
                            $str = "ffmpeg -i ".$url." -y -f mjpeg -ss ".$second." -t 1 -s ".$width."*".$height . $save_path . $name;//视频封面
                        }else{
                            $str = "ffmpeg -i ".$url." -y -f mjpeg -ss ".$second." -t 1 ". $save_path . $name;
                        }
                        break;
                    case 'gif':
                        if($width && $height){
                            $str = "ffmpeg -i ".$url." -t ".$second." -s ".$width."*".$height ." -pix_fmt rgb24 ". $save_path . $name;//gif
                        }else{
                            $str = "ffmpeg -i ".$url." -t ".$second." -f gif -r 10 ". $save_path . $name;//-r 10 帧率降到了10 fps
                        }
                        break;
                    default:
                        return ['code'=>500, 'msg'=>'参数错误'];
                        break;
                }
                exec($str, $output, $status);//执行上述语句
                $pic_path = $_SERVER['HTTP_ORIGIN']."/uploads/".$catalog."/".$name;//获取生成图路径
                return ['code'=>200, 'msg'=>'pic_path:视频封面图或gif图,video_url视频路径', 'pic_path'=>$pic_path, 'video_time'=>$vtime, 'video_url'=>$url];
            }else{
                return ['code'=>500, 'msg'=>'未获取视频'];
            }
        }catch (\Exception $e){
            return ['code'=>500, 'msg'=>$e->getMessage()];
        }
    }

}
