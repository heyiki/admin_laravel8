<?php
declare (strict_types = 1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\LoginService;
use App\Validates\Admin\LoginValidate;

class LoginController extends Controller
{
    /**
     * 显示资源列表
     *
     */
    public function index()
    {
        return view('admin/sys/login/index');
    }

    public function read()
    {
        try {
            $data = request()->post();
            $LoginValidate = new LoginValidate();

            if (!$LoginValidate->scene('login')->check($data)) {
                return $this->result('', 500, $LoginValidate->getError());
            }

            /*if(!$LoginValidate->check($data,'','','login')){
                return $this->result('', 500, $LoginValidate->getError());
            }*/

            $res = LoginService::login($data);
            return $this->result('', $res['code'], $res['msg']);
        } catch (\Exception $e) {
            return $this->result('', 500, $e->getMessage());
        }
    }

    /**
     * 退出
     * @return [type] [description]
     */
    public function out()
    {
        session()->flush();
        $url = url("admin/login");
        redirect($url)->send();
    }
}
