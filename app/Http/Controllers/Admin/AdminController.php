<?php
namespace App\Http\Controllers\Admin;

use App\Models\AdminLogModel;
use App\Models\AdminModel;
use App\Models\AdminRolesModel;
use App\Models\SystemMenusModel;
use App\Services\Admin\AdminService;
use App\Validates\Admin\SystemValidate;

class AdminController extends BaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        if(request()->ajax()){
            $page = request()->input("page", 1);
            if(request()->input("real_name")){
                $map[] = ['real_name','like',"%".request()->input("real_name")."%"];
            }
            if(request()->input("account")){
                $map[] = ['account','like',"%".request()->input("account")."%"];
            }
//            $map[] = ['is_del', '=', null];
            //超级管理员可以查看全部，否自身
            $getRoles = AdminModel::getRoles(self::$adminId);
            if($getRoles) {
                $map[] = ['id', '=', $getRoles];
            }

            $list = AdminModel::getList($this->pageSize,$page,$map);

            if($list['data']){
                foreach ($list['data'] as $k => $v) {
                    $list['data'][$k]['roles_name'] = $v['admin_roles']['title'];
                    $list['data'][$k]['create_time'] = date("Y-m-d H:i", $v['create_time']);
                    $list['data'][$k]['last_time'] = $v['last_time'] ? date("Y-m-d H:i", $v['last_time']) : '';
                }
            }
            return $this->rjson($list);
        }
        return view('admin/sys/admin/index');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $id = request()->input('id');
        if($id) {
            $res = AdminModel::find($id);
        }
        $roles = AdminRolesModel::all();
        return view('admin/sys/admin/modify',['roles'=>$roles,'model'=>$res]);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit()
    {
        try {
            $data = request()->post();
            $SystemValidate = new SystemValidate();
            if (!$SystemValidate->scene('admin')->check($data)) {
                return $this->result('', 500, $SystemValidate->getError());
            }
            $res = AdminService::editAdmin($data);
            return $this->result("", $res['code'], $res['msg']);
        } catch (\Exception $e) {
            return $this->result("", 500, $e->getMessage());
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete()
    {
        try{
            $ids = request()->post('id');
            $res = AdminService::delAdmin($ids);
            return $this->result("", $res['code'], $res['msg']);
        }catch (\Exception $e){
            return $this->result("", 500, $e->getMessage());
        }
    }

    /*********************************************角色管理*********************************************/
    /*************************************************************************************************/
    /*************************************************************************************************/
    public function roles()
    {
        if(request()->ajax()){
            $page = request()->get('page',1);
            if(request()->get("title")){
                $map[] = ['title','like',"%".request()->get("title")."%"];
            }
            $list = AdminRolesModel::getRoleList($this->pageSize, $page, $map);
            if($list['data']) {
                foreach ($list['data'] as $k => $v) {
                    $rules_name = "全部";
                    if($v['rules'] != "all"){
                        $rule = explode(',',$v['rules']);
                        $rules_name = SystemMenusModel::whereIn('id',$rule)->pluck('menu_name');
                    }
                    $list['data'][$k]['rules_name'] = $rules_name;
                }
            }
            return $this->rjson($list);
        }
        return view('admin/sys/roles/index');
    }

    public function roles_modify()
    {
        $id = request()->input('id');
        if($id){
            $res = AdminRolesModel::find($id);
            $res['rules'] = $res['rules'] != "all" ? $res['rules'] : "";
        }
        return view('admin/sys/roles/modify',['model'=>$res]);
    }

    /**
    * 保存更新的资源
    *
    * @param  \think\Request  $request
    * @param  int  $id
    * @return \think\Response
    */
    public function roles_update()
    {
        try {
            $data = request()->post();
            $SystemValidate = new SystemValidate();
            if (!$SystemValidate->scene('roles')->check($data)) {
                return $this->result('', 500, $SystemValidate->getError());
            }
            $res = AdminRolesModel::editRole($data);

            return $this->result('',$res['code'],$res['msg']);

        } catch (\Exception $e) {
            return $this->result("", 500, $e->getMessage());
        }
    }

    /**
     * 后台日志
     */
    public function log()
    {
        if(request()->ajax()){
            $page = request()->get('page',1);
            $map = [];
            $list = AdminLogModel::getLogList($this->pageSize,$page,$map);
            if($list['data']){
                foreach ($list['data'] as $k => $v) {
                    $list['data'][$k]['create_time'] = date("Y-m-d H:i", $v['create_time']);
                }
            }
            return $this->rjson($list);
        }
        return view('admin/sys/admin/log');
    }
}
