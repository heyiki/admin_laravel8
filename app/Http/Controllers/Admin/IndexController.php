<?php
namespace App\Http\Controllers\Admin;


use Illuminate\Support\Facades\DB;

class IndexController extends BaseController
{
    public function index()
    {
//        $this->assign("siteConfig", SystemConfigModel::getConfig());
        return view('admin/index/index');
    }

    public function welcome()
    {
        //新粉丝->总数
        /*$d_user_num = UserModel::count("uid");
        $first_line['d_user_num'] = $d_user_num;

        //新粉丝->月
        $m_user_num = UserModel::where('create_time', '>', $pre_month)->where('create_time', '<=', $now_day)->count("uid");
        $first_line['m_user_num'] = $m_user_num;

        $this->assign([
            'first_line' => $first_line,
        ]);*/

        return view('admin/public/welcome');
    }

    /*********************************通用修改*************************************/
    public function currEdit()
    {
    	try {
    		$inp = request()->all();
    		$check = DB::table($inp['table'])->where($inp['pk'],$inp['id'])->first($inp['pk']);
    		if(!$check) return $this->result("", 500, "暂无相关数据");
            if(isset($inp['checked'])){
                $map[$inp['field']] = $inp['checked'] == 'true' ? 1 : 0;
            }else{
                $map[$inp['field']] = $inp['value'];
            }
    		$res = DB::table($inp['table'])->where($inp['pk'],$inp['id'])->update($map);
    		if($res){
    			return $this->result("", 200, "操作成功");
    		}
    		return $this->result("", 500, "操作失败");
    	} catch (\Exception $e) {
    		return $this->result("", 500, $e->getMessage());
    	}
    }
}
