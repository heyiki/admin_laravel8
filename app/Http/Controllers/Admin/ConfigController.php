<?php
namespace App\Http\Controllers\Admin;

use App\Models\SystemConfigModel;

class ConfigController extends BaseController
{

    /**
     * 类型
    */
    protected $type = [
        'website' => '基础配置',
//        'store' => '商城配置',
//        'sms' => '短信配置',
//        'mail' => '邮箱配置',
//        'extract' => '提现配置',
//        'aliyun' => '阿里云配置',
//        'wechat' => '微信公众号配置',
//        'wechat_min' => '微信小程序配置',
//        'joinpay' => '汇聚支付',
//        'xcx_enjoy' => '小程序分享',
//        'live' => '直播配置',
    ];

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $type = request()->input('type', 'website');
        $view = $type != "website" ? $type : "index";

        $res = SystemConfigModel::getConfig($type);
        $res['logo'] = $res['logo'] ? explode(";", $res['logo']) : "";
        $res['logo_img'] = $res['logo_img'] ? explode(";", $res['logo_img']) : "";
        $res['icon'] = $res['icon'] ? explode(";", $res['icon']) : "";
        $res['head_img'] = $res['head_img'] ? explode(";", $res['head_img']) : "";

        return view("admin/sys/config/{$view}",['model'=>$res,'type'=>$type,'list'=>$this->type]);
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update()
    {
        try {
            $data = request()->post();
            $type = $data['type'];
            if($type == "website") {
                $data['logo'] = $data['logo'] ? implode(";", $data['logo']) : "";
                $data['logo_img'] = $data['logo_img'] ? implode(";", $data['logo_img']) : "";
                $data['icon'] = $data['icon'] ? implode(";", $data['icon']) : "";
            }
            if($type == 'xcx_enjoy'){
                $data['head_img'] = $data['head_img'] ? implode(";", $data['head_img']) : "";
            }
            unset($data['type']);
            $res = SystemConfigModel::saveConfig($type, $data);
            return $this->result("", $res['code'], $res['msg']);
        } catch (\Exception $e) {
            return $this->result("", 500, $e->getMessage());
        }
    }


}
