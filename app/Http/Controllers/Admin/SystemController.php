<?php
namespace App\Http\Controllers\Admin;

use App\Models\SystemMenusModel;
use App\Validates\Admin\SystemValidate;

class SystemController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        if(request()->ajax()){
            $list = SystemMenusModel::listAll();
            return response()->json(SystemMenusModel::generateTree($list));
        }
        return view('admin/sys/administrator/menus_list');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $id = request()->input('id');
        if($id){
            $res = SystemMenusModel::find($id);
        }

        $tree = SystemMenusModel::treeMenusList();

        return view("admin/sys/administrator/menus_modify",['model'=>$res,'tree'=>$tree]);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit()
    {
        try {
            $data = request()->post();
            $SystemValidate = new SystemValidate();
            if (!$SystemValidate->scene('menu')->check($data)) {
                return $this->result('', 500, $SystemValidate->getError());
            }

            $res = SystemMenusModel::editMenu($data);
            return $this->result("", $res['code'], $res['msg']);
        } catch (\Exception $e) {
            return $this->result("", 500, $e->getMessage());
        }
    }

    /**
     * IconFont 图标
    */
    public function unicode()
    {
        return view('admin/sys/administrator/unicode');
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete()
    {
        $ids = request()->input("id");
        $res = SystemMenusModel::delMenu($ids);
        return $this->result("", $res['code'], $res['msg']);
    }
}
