<?php
namespace App\Http\Controllers\Admin;

use App\Models\SystemConfigModel;

/**
 * 关于我们
 */
class AboutController extends BaseController
{
    /**
     * 用户协议
     *
     */
    public function index()
    {
        $type = 'agreement';
        $res = SystemConfigModel::where(['type'=>'about','name'=>$type])->first();
        return view('admin/about/index',['model'=>$res,'type'=>$type,'id'=>$res['id']]);
    }

    /**
     * 保存更新的资源
     */
    public function update()
    {
        try {
            $data = request()->post();
            $data['type'] = 'about';
            $data['update_time'] = time();
            $id = $data['id'];
            unset($data['id']);

            if($id){
                $res = SystemConfigModel::where('id',$id)->update($data);
            }else{
                $res = SystemConfigModel::create($data);
            }
            if($res){
                return $this->result('', 200, '操作成功');
            }
            return $this->result('', 500, '操作失败');

        } catch (\Exception $e) {
            return $this->result('', 500, $e->getMessage());
        }
    }


}
