<?php
namespace App\Http\Controllers\Admin;


use App\Models\ArticleCategoryModel;
use App\Models\ArticleModel;
use App\Validates\Admin\ArticleValidate;

class ArticleController extends BaseController
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        if(request()->ajax()){
            $page = request()->input("page",1);

            if(request()->input("cat_id")){
                $map[] = ['cat_id','like',"%,".request()->input('cat_id').",%"];
            }
            if(request()->input("param.info/s")){
                $map[] = ['title|keyword','like',"%".request()->input('info')."%"];
            }

            $list = ArticleModel::getArticleList($this->pageSize,$page,$map);

            if($list['data']){
                foreach ($list['data'] as $k => $v) {
                    $list['data'][$k]['create_time'] = date("Y-m-d H:i",$v['create_time']);
                }
            }
            return $this->rjson($list);
        }

        $tree = ArticleCategoryModel::treeList();

        return view('admin/article/index',['tree'=>$tree]);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $id = request()->input('id');
        if($id) {
            $res = ArticleModel::find($id);
            $res['picture'] = $res['picture'] ? explode(";", $res['picture']) : "";
            $cate_id = rtrim(substr($res['cat_id'], 1), ",");
        }
        return view('admin/article/modify',['model'=>$res,'cate_id'=>$cate_id]);
    }


    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit()
    {
        try {
            $data = request()->post();

            $ArticleValidate = new ArticleValidate();
            if (!$ArticleValidate->scene('article')->check($data)) {
                return $this->result('', 500, $ArticleValidate->getError());
            }

            if(request()->input("picture")){
                $data['picture'] = implode(";",request()->input("picture"));
            }

            $cat_id = explode(',',$data['cat_id']);
            $cat_name = ArticleCategoryModel::whereIn('id',$cat_id)->pluck("cat_name")->toArray();
            if($cat_name) $data['cat_name'] = implode(",",$cat_name);
            $data['cat_id'] = ",".$data['cat_id'].",";

            $res = ArticleModel::editArticle($data);

            return $this->result('',$res['code'],$res['msg']);

        } catch (\Exception $e) {
            return $this->result("",500,$e->getMessage());
        }
    }

    /**
     * 文章分类
     */
    public function cate()
    {
        try {
            $cate = ArticleModel::cateList(ArticleCategoryModel::treeList());
            return response()->json($cate);
        } catch (\Exception $e) {
            return $this->result("",500,$e->getMessage());
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete()
    {
        try {
            $ids = request()->input("id");
            $res = ArticleModel::destroy($ids);
            if($res){
                return $this->result('',200,'操作成功');
            }
            return $this->result('',500,'操作失败');
        } catch (\Exception $e) {
            return $this->result('',500,$e->getMessage());
        }
    }
}
