<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 返回封装后的API数据到客户端
     * @access protected
     * @param mixed $data 要返回的数据
     * @param integer $code 返回的code
     * @param mixed $msg 提示信息
     * @return void
     */
    protected function result($data = [], $code = 200, $msg = 'OK')
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'data' => ($data ? : []),
            'time' => date("Y-m-d H:i", time()),
        ];
//        return json_encode($result);
        return response()->json($result);
    }
}
