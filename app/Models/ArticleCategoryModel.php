<?php
namespace App\Models;

/**
 * 文章分类
 * @mixin think\Model
 */
class ArticleCategoryModel extends BaseModel
{
	use SoftDeletes;
    protected $table = 'article_category';
    protected $fillable = ['pid','cat_name','picture','is_show','sort'];

    public static function listAll()
	{
		return self::orderBy('sort','asc')->get()->toArray();
	}

	public static function treeList()
	{
		return self::cateTree(self::listAll());
	}

	/**
     * 添加编辑文章分类
     */
	public static function editArticleCategory($data)
    {
        try{
            $id = $data['id'];
            unset($data['id']);
            if(!empty($id)){
                $res = self::where('id',$id)->update($data);
            }else{
                $res = self::create($data);
            }
            if($res) {
                return self::result("", 200, "操作成功");
            }
            return self::result('',500,'操作失败');

        }catch (\Exception $e){
            return self::result('',500,$e->getMessage());
        }
    }
}

