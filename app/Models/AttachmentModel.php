<?php
declare (strict_types = 1);

namespace App\Models;

/**
 * 图片管理
 */
class AttachmentModel extends BaseModel
{
    use SoftDeletes;
    protected $table = 'attachment';
    protected $fillable = ['name','att_dir','satt_dir','att_size','att_type','att_mime','width','height','module_type','create_time'];
    const UPDATED_AT = null;

    /**
     * 获取图片管理列表
     */
    public static function getPictureList($pageSize, $page, $map)
    {
        $res = self::select(['id','name','att_dir','satt_dir'])
            ->where($map)
            ->orderBy('create_time','desc')
            ->paginate($pageSize,[],'page', $page);

        return $res;
    }

    /**
     * 删除指定图片
     */
    public static function delPicture($ids){
        try {
            self::startTrans();
            $res = self::destroy($ids);
            self::commit();
            if($res){
                return self::result("", 200, '操作成功');
            }
            return self::result("", 500, '操作失败');
        } catch (\Exception $e) {
            self::rollback();
            return self::result("", 500, $e->getMessage());
        }
    }

    /**
     * 将文件存储至数据库
     * 1、目前目录、上传来源、上传模块未做扩展，
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public static function attachment($data = [], $is_type)
    {
        if(empty($data)) return false;
        try {
            self::startTrans();
            $data['cat_id'] = 0; //目录分类ID
            $data['source'] = 1; //来源 1、本地
            $data['module_type'] = $is_type; //模块 1、后台 2、用户
            $data['create_time'] = time();
            self::create($data);
            self::commit();
            return true;
        } catch (\Exception $e) {
            self::rollback();
            return false;
        }
    }

}
