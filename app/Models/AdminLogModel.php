<?php
declare (strict_types = 1);

namespace App\Models;

/**
 * 管理员日志表
 */
class AdminLogModel extends BaseModel
{
    protected $table = 'admin_log';
    protected $fillable = ['admin_id','admin_name','path','ip','param'];
    const UPDATED_AT = null;

    /**
     * 获取日志列表
     * @param $pageSize
     * @param $page
     * @param $map
     * @return mixed
     */
    public static function getLogList($pageSize, $page, $map){
        return self::select('*')
            ->where($map)
            ->orderBy('create_time','desc')
            ->paginate($pageSize,[],'page', $page)
            ->toArray();
    }

    public static function record()
    {
        $admin = session("adminInfo");
        if(!strpos(request()->url(),'page') && !strpos(request()->url(),'index')) {
            if($admin['id'] && $admin['account']) {
                $log['admin_id'] = $admin['id'];
                $log['admin_name'] = $admin['account'];
                $log['path'] = request()->url();
                $log['ip'] = request()->ip();
                $log['param'] = json_encode(request()->param());
                self::create($log);
            }
        }
    }
}
