<?php
declare (strict_types = 1);

namespace App\Models;

/**
 * 用户地址表
 */
class UserAddressModel extends BaseModel
{
    use SoftDeletes;
    protected $table = 'user_address';


}
