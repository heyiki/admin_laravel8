<?php
namespace App\Models;

/**
 * 文章列表
 * @mixin think\Model
 */
class ArticleModel extends BaseModel
{
    use SoftDeletes;
    protected $table = 'article';

    /**
     * 获取文章列表
     */
    public static function getArticleList($pageSize, $page, $map){

        $list = self::select('*')
            ->where($map)
            ->orderBy('create_time','desc')
            ->paginate($pageSize,[],'page', $page)->toArray();

        return $list;
    }

    /**
     * 添加编辑文章
     */
    public static function editArticle($data)
    {
        try{
            $id = $data['id'];
            unset($data['id']);
            if(!empty($id)){
                $res = self::where('id',$id)->update($data);
            }else{
                $res = self::create($data);
            }
            if($res) {
                return self::result("", 200, "操作成功");
            }
            return self::result('',500,'操作失败');

        }catch (\Exception $e){
            return self::result('',500,$e->getMessage());
        }
    }

	/**
	 * 转为多选下拉框格式
	 * [{"id":12,"name":"长者","status":0},{"id":12,"name":"长者","status":1}]
	 * @param  array  $data [description]
	 * @return [type]       [description]
	 */
    public static function cateList($data = [])
    {
    	if(empty($data)) return false;
    	foreach ($data as $k => $v) {
    		if($v['id'] && $v['cat_name']){
    			$arr[] = [
    				'id' => $v['id'],
    				'name' => $v['lefthtml'].$v['cat_name'],
    				'status' => $v['pid'] == 0 ? 0 : 1,   //顶级分类不可选
    			];
    		}
    	}
    	return $arr;
    }
}
