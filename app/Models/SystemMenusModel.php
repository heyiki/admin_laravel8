<?php
declare (strict_types = 1);

namespace App\Models;

/**
 * 管理员表
 */
class SystemMenusModel extends BaseModel
{
    protected $table = 'system_menus';
    const CREATED_AT = null;

    /**
     * 权限添加编辑
     * @param array $data
     * @return array|void
     */
    public static function editMenu($data = []){
        try{
            $id = $data['id'];
            unset($data['id']);
            self::startTrans();
            if($id){
                $res = self::where('id',$id)->update($data);
            }else{
                $data['module'] = "admin";
                $res = self::create($data);
            }
            if($res) {
                self::commit();
                return self::result("", 200, "操作成功");
            }
            return self::result("", 500, "操作失败");

        }catch (\Exception $e){
            self::rollback();
            return self::result('',500,$e->getMessage());
        }
    }

    /**
     * 权限删除
     * @param $ids
     * @return array|void
     */
    public static function delMenu($ids){
        try {
            $check = SystemMenusModel::where("pid",$ids)->first();
            if($check){
                return self::result('',500,'请删除该分类子菜单');
            }
            self::startTrans();
            $res = SystemMenusModel::destroy($ids);
            if($res) {
                self::commit();
                return self::result("", 200, "删除成功");
            }
            return self::result("", 500, "删除失败");
        } catch (\Exception $e) {
            self::rollback();
            return self::result("",500,$e->getMessage());
        }
    }

    /**
     * 获取图标
     * @param string $icon
     * @return void
     */
    public static function getIcon($icon = '')
    {
        if ($icon) {
            $str = explode(",", $icon);
            return $str[1];
        }
        return "&#xe6a7;";
    }

    /**
     * 处理请求参数
     * @param  [type] $url   请求地址：/admin/order.order/refund.html&id=3&type=ok
     * @param  [type] $param 权限参数：/admin/order.order/refund.html&id=&type=ok
     * @return [type]        [description]
     */
    public static function handleParam($url, $param)
    {
        $u = explode("&", $url);
        $p = explode("&", $param);
        $new_param = "";
        if(count($p) == count($u)){
            $arr = "";
            foreach ($p as $k => $v) {
                $str1 = explode("=", $v);
                if(empty($str1[1]) && $k != 0) {
                    foreach ($u as $k1 => $v1) {
                        $u_str = explode("=", $v1);
                        if($u_str[0] == $str1[0]){
                            $arr .= "&".$u_str[0]."=".$u_str[1];
                        }
                    }
                }else {
                    if($k != 0) {
                        $arr .= "&".$v;
                    }
                }
            }
            $new_param = $arr;
        }
        return $new_param ? $p[0].$new_param : $param;
    }


    public static function listAll()
    {
        return self::orderBy('sort','asc')->get()->toArray();
    }

    public static function treeMenusList()
    {
        return self::cateTree(self::listAll());
    }
}
