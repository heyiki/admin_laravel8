<?php
declare (strict_types = 1);

namespace App\Models;

/**
 * 角色表
 * @mixin \think\BaseModel
 */
class AdminRolesModel extends BaseModel
{
    protected $table = 'admin_roles';

    protected $fillable = ['title','description','rules'];

//    public static function all()
//    {
//        return self::where("status", 1)->orderBy("id","asc")->select()->get()->toArray();
//    }

    /**
     * 角色列表
     */
    public static function getRoleList($pageSize, $page, $map){
        return self::select('*')
            ->where($map)
            ->orderBy('update_time','desc')
            ->orderBy('create_time','desc')
            ->paginate($pageSize,[],'page', $page)
            ->toArray();
    }

    /**
     * 角色添加编辑
     * @param array $data
     * @return array|void
     */
    public static function editRole($data = []){
        try{
            $data['rules'] = $data['ids'] == "false" ? "all" : substr($data['ids'], 1);
            $data['update_time'] = time();
            $id = $data['id'];
            unset($data['ids'],$data['id']);
            self::startTrans();
            if($id){
                $res = self::where('id',$id)->update($data);
            }else{
                $data['create_time'] = time();
                $res = self::create($data);
            }
            if($res) {
                self::commit();
                return self::result("", 200, "操作成功");
            }
            return self::result("", 500, "操作失败");

        }catch (\Exception $e){
            self::rollback();
            return self::result('',500,$e->getMessage());
        }
    }
}
