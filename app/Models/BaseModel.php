<?php
declare (strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 基础model
 */
class BaseModel extends Model
{
    //自定义存储时间戳的字段名
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    //自定义软删除字段
//    const DELETED_AT = 'is_del';

    public $timestamps = true;

    protected $guarded = [];//不可以注入数据字段

    /**
     * 数据库获取UTC默认时间格式转换成时间戳
     * @param \DateTimeInterface $date
     * @return int|string
     */
    protected function serializeDate($date)
    {
        return $date->getTimestamp();
    }

    /**
     * 获取当前时间
     *
     * @return int
     */
    public function freshTimestamp() {
        return time();
    }

    /**
     * 避免转换时间戳为时间字符串
     *
     * @param DateTime|int $value
     * @return DateTime|int
     */
    public function fromDateTime($value) {
        return $value;
    }

    /**
     * select的时候避免转换时间为Carbon
     *
     * @param mixed $value
     * @return mixed
     */
//    protected function asDateTime($value) {
//        return $value;
//    }

    /**
     * 从数据库获取的为获取时间戳格式
     *
     * @return string
     */
    public function getDateFormat() {
        return 'U';
    }

    /**
     * 事物开启
     */
    final public static function startTrans()
    {
        return DB::beginTransaction();
    }

    /**
     * 事物提交
     */
    final public static function commit()
    {
        return DB::commit();
    }

    /**
     * 事物回滚
     */
    final public static function rollback()
    {
        return DB::rollBack();
    }

    /**
     * 高效遍历子集
     * @param  [type] $list   待处理数组
     * @return [type]        [description]
     */
    public static function generateTree($list = []){
        if(!$list) return [];
        $items = [];
        foreach($list as $v){
            $items[$v['id']] = $v;
        }
        $tree = [];
        foreach($items as $k => $item){
            $items[$k]['open'] = true;
            $items[$k]['icon'] = $item['icon'] ? explode(",",$item['icon']) : "";
            if(isset($items[$item['pid']])){
                $items[$item['pid']]['children'][] = &$items[$k];
            }else{
                $tree[] = &$items[$k];
            }
        }
        return $tree;
    }

    /**
     * 无限分类
     * @param $list            栏目
     * @param string $lefthtml 分隔符
     * @param int $pid         父ID
     * @param int $lvl         层级
     * @return array
     */
    public static function cateTree($list , $lefthtml = '|— ' , $pid = 0 , $level = 0 ){
        if(!$list) return [];
        $arr = [];
        foreach ($list as $v){
            if ($v['pid'] == $pid) {
                $v['level']      = $level + 1;
                $v['lefthtml'] = str_repeat($lefthtml, $level);
                $arr[] = $v;
                $arr = array_merge($arr, self::cateTree($list, $lefthtml, $v['id'], $level+1));
            }
        }
        return $arr;
    }

    /**
     * 操作成功返回值函数
     * @param string $data 数据组
     * @param integer $code  状态码
     * @param string $msg  提示语
     * @return void
     */
    public static function result($data = "", $code = 200, $msg = 'OK')
    {
        return  [
            'code' => $code,
            'msg' => $msg,
            'data' => ($data ? $data : [])
        ];
    }
}
