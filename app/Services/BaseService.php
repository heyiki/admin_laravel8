<?php
namespace App\Services;

use Illuminate\Database\Eloquent\Model;

/**
 * 服务类-基类
 */
class BaseService extends Model
{
    /**
     * 构造方法
     * @author zongjl
     * @date 2019/5/23
     */
    public function __construct()
    {
        parent::__construct();
        // TODO...
    }

}
