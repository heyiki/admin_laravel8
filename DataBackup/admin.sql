/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 127.0.0.1:3306
 Source Schema         : admin

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 31/05/2021 17:51:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hk_admin
-- ----------------------------
DROP TABLE IF EXISTS `hk_admin`;
CREATE TABLE `hk_admin`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `password` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码，password_hash加密  password_verify验证',
  `real_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `roles_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分组ID',
  `last_ip` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后一次登录ip',
  `last_time` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '最后一次登录时间',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `login_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录次数',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态 1有效0无效',
  `unique` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '唯一值',
  `update_time` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '更新时间',
  `is_del` int(11) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `account`(`account`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '后台管理员表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_admin
-- ----------------------------
INSERT INTO `hk_admin` VALUES (1, 'admin', '$2y$10$MtCHCpxVQOJtyxKgmSfJ5u2T2q6WEEsxIGSRSCaBt.ndpAJISTEMq', '管理员', '1', '127.0.0.1', 1622441501, 1588841382, 40, 1, '680b615bb4e61a79d94aeef600e30145', 1622441501, 0);

-- ----------------------------
-- Table structure for hk_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `hk_admin_log`;
CREATE TABLE `hk_admin_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '管理员操作记录ID',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `admin_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员姓名',
  `path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '链接',
  `ip` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录IP',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '操作时间',
  `param` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '参数',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `admin_id`(`admin_id`) USING BTREE,
  INDEX `create_time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员操作记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for hk_admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `hk_admin_roles`;
CREATE TABLE `hk_admin_roles`  (
  `id` smallint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分组id',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标题',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  `rules` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '规则',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '添加时间',
  `update_time` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员分组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_admin_roles
-- ----------------------------
INSERT INTO `hk_admin_roles` VALUES (1, '超级管理员', 1, 'all', '超级管理员', 1589956371, 1621046380);
INSERT INTO `hk_admin_roles` VALUES (2, '测试', 1, '1,15,16', '测试', 1589956371, 1622430880);

-- ----------------------------
-- Table structure for hk_article
-- ----------------------------
DROP TABLE IF EXISTS `hk_article`;
CREATE TABLE `hk_article`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cat_id` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '分类id',
  `cat_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分类名称',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '作者',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '描述',
  `details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `click` mediumint(8) UNSIGNED NULL DEFAULT 0 COMMENT '浏览次数',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '原文链接',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态，1显示  0隐藏',
  `is_hot` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否热门',
  `keyword` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '关键字',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `update_time` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '更新时间',
  `is_del` int(11) NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_article
-- ----------------------------

-- ----------------------------
-- Table structure for hk_article_category
-- ----------------------------
DROP TABLE IF EXISTS `hk_article_category`;
CREATE TABLE `hk_article_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '父id',
  `cat_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `picture` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `sort` smallint(5) NOT NULL DEFAULT 50 COMMENT '排序',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否显示',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `update_time` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '更新时间',
  `is_del` int(11) NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品分类表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_article_category
-- ----------------------------
INSERT INTO `hk_article_category` VALUES (1, 0, '新闻中心', '', 50, 1, 1590480230, 0, 0);
INSERT INTO `hk_article_category` VALUES (2, 1, '企业新闻', '', 50, 1, 1590480256, 0, 0);
INSERT INTO `hk_article_category` VALUES (3, 1, '行业新闻', '', 50, 1, 1590480276, 1622449426, 0);

-- ----------------------------
-- Table structure for hk_attachment
-- ----------------------------
DROP TABLE IF EXISTS `hk_attachment`;
CREATE TABLE `hk_attachment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NULL DEFAULT 0 COMMENT '分类目录ID',
  `name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '名称',
  `att_dir` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路径',
  `satt_dir` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '压缩路径',
  `att_size` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '大小',
  `att_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类型',
  `att_mime` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'mime类型',
  `width` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原宽度',
  `height` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原高度',
  `source` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '图片上传类型 1本地 2七牛云 3OSS 4COS ',
  `module_type` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '图片上传模块类型 1 后台上传 2 用户生成',
  `create_time` int(11) NULL DEFAULT 0 COMMENT '上传时间',
  `is_del` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '软删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for hk_attachment_category
-- ----------------------------
DROP TABLE IF EXISTS `hk_attachment_category`;
CREATE TABLE `hk_attachment_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT 0 COMMENT '父级ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件分类表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_attachment_category
-- ----------------------------

-- ----------------------------
-- Table structure for hk_banner
-- ----------------------------
DROP TABLE IF EXISTS `hk_banner`;
CREATE TABLE `hk_banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片名称',
  `position` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '位置 \r\n1-首页轮播',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否显示',
  `sort` smallint(5) NOT NULL DEFAULT 50 COMMENT '排序',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '地址',
  `update_time` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '更新时间',
  `is_del` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '软删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '轮播图表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_banner
-- ----------------------------

-- ----------------------------
-- Table structure for hk_system_config
-- ----------------------------
DROP TABLE IF EXISTS `hk_system_config`;
CREATE TABLE `hk_system_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配置字段键值对',
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '配置键值',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `update_time` int(11) NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基本设置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_system_config
-- ----------------------------

-- ----------------------------
-- Table structure for hk_system_menus
-- ----------------------------
DROP TABLE IF EXISTS `hk_system_menus`;
CREATE TABLE `hk_system_menus`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `menu_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `module` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '模块名',
  `controller` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '控制器',
  `action` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '方法名',
  `params` varchar(225) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '参数',
  `sort` smallint(5) NOT NULL DEFAULT 50 COMMENT '排序',
  `is_show` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否显示',
  `update_time` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hk_system_menus
-- ----------------------------
INSERT INTO `hk_system_menus` VALUES (1, 0, 'set,&#xe6ae;', '设置', 'admin', 'set', 'index', '', 999, 1, 0);
INSERT INTO `hk_system_menus` VALUES (2, 1, 'personal-center,&#xe70b;', '管理权限', 'admin', 'sys', 'index', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (3, 2, '', '管理员', 'admin', 'admin', 'index', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (4, 3, '', '查看', 'admin', 'admin', 'create', '&id=', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (5, 3, '', '添加/编辑', 'admin', 'admin', 'edit', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (6, 3, '', '删除', 'admin', 'admin', 'delete', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (7, 2, '', '系统日志', 'admin', 'admin', 'log', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (8, 2, '', '角色管理', 'admin', 'admin.role', 'roles', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (9, 8, '', '查看', 'admin', 'admin.role', 'roles_modify', '&id=', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (10, 8, '', '添加/编辑', 'admin', 'admin.role', 'roles_update', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (11, 2, '', '权限规则', 'admin', 'admin.system', 'index', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (12, 11, '', '查看', 'admin', 'admin.system', 'create', '&id=', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (13, 11, '', '添加/编辑', 'admin', 'admin.system', 'edit', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (14, 11, '', '删除', 'admin', 'admin.system', 'delete', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (15, 1, 'ViewGallery,', '系统设置', 'admin', 'config', 'index', '&type=', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (16, 15, '', '更新', 'admin', 'config', 'update', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (17, 1, 'download,&#xe714;', '数据备份', 'admin', 'databackup', 'index', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (18, 17, '', '备份/优化/修复', 'admin', 'databackup', 'read', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (19, 17, '', '还原', 'admin', 'databackup', 'import', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (20, 17, '', '下载', 'admin', 'databackup', 'downloadFile', '&time=', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (21, 17, '', '删除', 'admin', 'databackup', 'delFile', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (25, 0, 'nav-list,&#xe6fa;', '内容', 'admin', 'article', 'index', '', 2, 1, 0);
INSERT INTO `hk_system_menus` VALUES (26, 25, '', '轮播图管理', 'admin', 'banner', 'index', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (27, 26, '', '查看', 'admin', 'banner', 'create', '&id=', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (28, 26, '', '添加/编辑', 'admin', 'banner', 'edit', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (29, 26, '', '删除', 'admin', 'banner', 'delete', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (30, 25, '', '关于我们', 'admin', 'about', 'index', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (31, 30, '', '更新', 'admin', 'about', 'update', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (32, 30, '', '用户协议', 'admin', 'about', 'index', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (40, 25, '', '文章管理', 'admin', 'article.article', 'index', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (41, 40, '', '查看', 'admin', 'article.article', 'create', '&id=', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (42, 40, '', '添加/编辑', 'admin', 'article.article', 'edit', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (43, 40, '', '删除', 'admin', 'article.article', 'delete', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (44, 25, '', '文章分类', 'admin', 'article.category', 'index', '', 50, 1, 0);
INSERT INTO `hk_system_menus` VALUES (45, 44, '', '查看', 'admin', 'article.category', 'create', '&id=', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (46, 44, '', '添加/编辑', 'admin', 'article.category', 'edit', '', 50, 0, 0);
INSERT INTO `hk_system_menus` VALUES (47, 44, '', '删除', 'admin', 'article.category', 'delete', '', 50, 0, 0);

SET FOREIGN_KEY_CHECKS = 1;
