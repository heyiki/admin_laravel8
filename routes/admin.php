<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//会员管理
Route::get('user/index','UserController@index');//会员列表
Route::get('user/create','UserController@create');//查看
Route::post('user/edit','UserController@edit');//添加编辑
//Route::post('user/delete','UserController@delete');//删除
Route::get('user/balance','UserController@balance');//余额、积分
Route::post('user/update_balance','UserController@update_balance');//更新余额、积分
Route::get('user/read','UserController@read');//详情
Route::get('user/balanceInfo','UserController@balanceInfo');//余额明细列表
Route::get('user/integralInfo','UserController@integralInfo');//积分明细列表
Route::post('user/exportExcel','UserController@exportExcel');//会员excel导出

//文章管理
//文章列表
Route::get('article.article/index','ArticleController@index');
Route::get('article.article/create','ArticleController@create');//查看
Route::post('article.article/edit','ArticleController@edit');//添加编辑
Route::post('article.article/delete','ArticleController@delete');//删除
Route::post('article.article/cate','ArticleController@cate');//加载分类
//文章分类
Route::any('article.category/index','ArticleCategoryController@index');
Route::get('article.category/create','ArticleCategoryController@create');//查看
Route::post('article.category/edit','ArticleCategoryController@edit');//添加编辑
Route::post('article.category/delete','ArticleCategoryController@delete');//删除

//轮播图
Route::get('banner/index','BannerController@index');
Route::get('banner/create','BannerController@create');//查看
Route::post('banner/edit','BannerController@edit');//添加编辑
Route::post('banner/delete','BannerController@delete');//删除

//关于我们
Route::get('about/index','AboutController@index');
Route::post('about/update','AboutController@update');//更新

//系统设置
Route::get('config/index','ConfigController@index');
Route::post('config/update','ConfigController@update');//更新

/************************************************ 以下路由基本不变 ******************************************************/
//登录页
Route::get('login', 'LoginController@index');
//登录操作
Route::post('signin', 'LoginController@read');
//退出登录
Route::get('logout', 'LoginController@out');

//后台主页
Route::get('index', 'IndexController@index');
Route::get('welcome', 'IndexController@welcome');

//后台列表按钮编辑
Route::post('index/currEdit','IndexController@currEdit');

//数据备份
Route::get('databackup/index','DatabackupController@index');
Route::get('databackup/create','DatabackupController@create');//查看数据库列表
Route::post('databackup/read','DatabackupController@read');//备份
Route::post('databackup/import','DatabackupController@import');//还原
Route::get('databackup/downloadFile','DatabackupController@downloadFile');//下载
Route::post('databackup/delFile','DatabackupController@delFile');//删除

//图片管理
Route::get('images/index','ImagesController@index');//展示
Route::post('images/create','ImagesController@create');//上传
Route::post('images/read','ImagesController@read');//选择
Route::post('images/delete','ImagesController@delete');//删除

//权限列表
Route::any('admin.system/index','SystemController@index');
Route::get('admin.system/create','SystemController@create');//查看
Route::post('admin.system/edit','SystemController@edit');//添加编辑
Route::post('admin.system/delete','SystemController@delete');//删除
Route::get('admin.system/unicode','SystemController@unicode');//IconFont 图标

//管理权限
//管理员
Route::get('admin/index','AdminController@index');
Route::get('admin/create','AdminController@create');//查看
Route::post('admin/edit','AdminController@edit');//添加编辑
Route::post('admin/delete','AdminController@delete');//删除
Route::get('admin/log','AdminController@log');//日志

//角色
Route::get('admin.role/roles','AdminController@roles');
Route::get('admin.role/roles_modify','AdminController@roles_modify');//查看
Route::post('admin.role/roles_update','AdminController@roles_update');//添加编辑
